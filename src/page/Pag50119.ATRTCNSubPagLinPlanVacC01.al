page 50119 "ATRTCNSubPagLinPlanVacC01"
{
    Caption = 'Sub Pagina Lienas Planificacion Vacunas';
    PageType = ListPart;
    SourceTable = ATRTCNLinPlanVacunC01;
    AutoSplitKey = true;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Codigo field.';
                    ApplicationArea = All;
                    Editable = false;
                    Visible = false;
                }
                field("NºLinea"; Rec."NºLinea")
                {
                    ToolTip = 'Specifies the value of the N ºLinea field.';
                    ApplicationArea = All;
                    Editable = false;
                    Visible = false;
                }
                field(ClienteAVacunar; Rec.ClienteAVacunar)
                {
                    ToolTip = 'Specifies the value of the Cliente A Vacunar field.';
                    ApplicationArea = All;
                    Editable = true;
                }
                field(FechaVacunacion; Rec.FechaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha Vacunacion field.';
                    ApplicationArea = All;
                    Editable = true;
                }
                field(TipoVacuna; Rec.DescripcionOtrosF(eATRTCNTipoVacunaC01::Vacuna, Rec.CodigoVacuna))
                {
                    ToolTip = 'Specifies the value of the Tipo Vacuna field.';
                    ApplicationArea = All;
                    Editable = true;
                }
                field(TipoOtros; Rec.DescripcionOtrosF(eATRTCNTipoVacunaC01::Otros, Rec.CodigoOtros))
                {
                    ToolTip = 'Specifies the value of the Tipo Otros field.';
                    ApplicationArea = All;
                    Editable = true;
                }
                field(NombreClienteaVacunar; Rec.NombreClienteF())
                {
                    Caption = 'Nombre del cliente a vacunar';
                    ApplicationArea = All;
                    Editable = true;
                }
                field(FechaSiguienteVacunacion; Rec.FechaSiguienteVacunacion)
                {
                    ToolTip = 'Fecha de Vacunacion siguiente';
                    ApplicationArea = All;
                    Editable = true;
                }
            }
        }
    }
    var
        eATRTCNTipoVacunaC01: Enum ATRTCNTipoVacunaC01;

    // trigger OnNewRecord(BelowxRec: Boolean)
    // var
    //     rlATRTCNCabeceraPlanVacunaC01: Record ATRTCNCabeceraPlanVacunaC01;
    // begin
    //     if (rlATRTCNCabeceraPlanVacunaC01.Get(rec.Codigo)) then begin
    //         rec.Validate(FechaVacunacion, rlATRTCNCabeceraPlanVacunaC01."FechaIncioVacu.Planif.");
    //     end;
    // end;
}