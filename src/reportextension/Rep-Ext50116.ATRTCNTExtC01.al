reportextension 50116 "ATRTCNTExtC01" extends "Sales - Shipment"
{
    dataset
    {
        addlast("Sales Shipment Header")
        {
            dataitem("SalesShipmentHeader"; "Sales Shipment Header")
            {

                column(ShiptoName_SalesShipmentHeader; "Ship-to Name")
                {
                }
                column(ShiptoCity_SalesShipmentHeader; "Ship-to City")
                {
                }
                column(ShiptoAddress_SalesShipmentHeader; "Ship-to Address")
                {
                }
            }
        }
    }

}
