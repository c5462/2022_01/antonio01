table 50113 "ATRTCNCabeceraPlanVacunaC01"
{
    Caption = 'Cabecera Plan de Vacunación';
    DataClassification = SystemMetadata;
    LookupPageId = ATRTCNListaCabPlanVacC01;
    fields
    {
        field(1; Codigo; Code[20])
        {
            Caption = 'Codigo Vacuna';
            DataClassification = SystemMetadata;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripcion Vacuna';
            DataClassification = SystemMetadata;
        }
        field(3; "FechaIncioVacu.Planif."; Date)
        {
            Caption = 'Fecha Incio Vacunación Planificada';
            DataClassification = SystemMetadata;
        }
        field(4; Empresavacunadora; Code[20])
        {
            Caption = 'Empresa Vacunadora';
            TableRelation = Vendor."No.";
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; Codigo)
        {
            Clustered = true;
        }
    }

    procedure NombreEmpresaVacunadoraF(): Text
    Var
        rlVendor: Record Vendor;
    begin
        if rlVendor.Get(Rec.Empresavacunadora) then begin
            exit(rlVendor.Name);
        end;
    end;

    trigger OnDelete()
    var
        rlATRTCNLinPlanVacunC01: Record ATRTCNLinPlanVacunC01;
    begin
        rlATRTCNLinPlanVacunC01.SetRange(Codigo, Rec.Codigo);
        rlATRTCNLinPlanVacunC01.DeleteAll(true);
    end;
}
