pageextension 50113 "ATRTCNCustomerListExtC01" extends "Customer List"
{
    actions
    {
        addfirst(General)
        {
            action(ATRTCNSwitchVariableC01)
            {
                Caption = 'Prueba Variables globales a BC';
                ApplicationArea = all;
                trigger OnAction()
                var
                    clMensaje: Label 'El valor actual es %1. Lo cambiamos a %2';
                begin
                    Message(clMensaje, cuATRFuncionesAppC01.SwitchF(), not cuATRFuncionesAppC01.SwitchF());
                    cuATRFuncionesAppC01.SwitchF(not cuATRFuncionesAppC01.SwitchF());
                end;
            }
            action(ATRExportClienteSeleccC01)
            {
                ApplicationArea = all;
                Caption = 'Exportar datos cliente seleccionado';
                trigger OnAction()
                begin
                    ExportaClienteSelecF();
                end;
            }
            action(ATRSumatorio01C01)
            {
                applicationArea = All;
                Caption = 'Sumatorio01';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    if rlDetailedCustLedgEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlDetailedCustLedgEntry.Amount;
                        until rlDetailedCustLedgEntry.Next() = 0;
                    end;
                    Message('Total=%1. Ha tardado (%2)', xlTotal, CurrentDateTime - xlInicio);
                end;
            }
            action(ATRDAENSumatorio02C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio02';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlDetailedCustLedgEntry.CalcSums(Amount);
                    xlTotal := rlDetailedCustLedgEntry.Amount;
                    Message('Total=%1. Ha tardado (%2)', xlTotal, CurrentDateTime - xlInicio);
                end;
            }

            action(ATRDAENSumatorio03C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio03';
                trigger OnAction()
                var
                    rlCustLedgEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgEntry.SetLoadFields(Amount); // Importante para rendimiento 
                    rlCustLedgEntry.SetAutoCalcFields(Amount); // Importante para rendimiento 
                    if rlCustLedgEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlCustLedgEntry.Amount;
                        until rlCustLedgEntry.Next() = 0;
                    end;
                    Message('Total=%1. Ha tardado (%2)', xlTotal, CurrentDateTime - xlInicio);
                end;
            }

            action(ATRDAENSumatorio04C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio04';
                trigger OnAction()
                var
                    rlCustLedgEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgEntry.CalcSums(Amount);
                    Message('Total=%1. Ha tardado (%2)', xlTotal, CurrentDateTime - xlInicio);
                end;
            }
            action(ATRExportarExcelC01)
            {
                ApplicationArea = all;
                Caption = 'Exportar excel';
                Image = Excel;
                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    xcolum: Integer;
                    xfila: Integer;
                begin
                    TempLExcelBuffer.CreateNewBook('Clientes');
                    //Rellenar las celdas
                    //Cabecera
                    AsignaCeldaF(1, 1, Rec.FieldCaption("No."), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 2, Rec.FieldCaption(Name), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 3, Rec.FieldCaption("Responsibility Center"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 4, Rec.FieldCaption("Location Code"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 5, Rec.FieldCaption("Phone No."), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 6, Rec.FieldCaption(Contact), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 7, Rec.FieldCaption("Balance (LCY)"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 8, Rec.FieldCaption("Balance Due (LCY)"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 9, Rec.FieldCaption("Sales (LCY)"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 10, Rec.FieldCaption("Payments (LCY)"), true, true, TempLExcelBuffer);
                    SetSelectionFilter(rlCustomer); //ASignamos las selecciones
                    //Bucle para rellenar
                    xfila := 1;
                    rlCustomer.SetAutoCalcFields("Balance (LCY)");
                    rlCustomer.SetLoadFields("No.", Name, Address, "Balance (LCY)", "Sales (LCY)", "Payments (LCY)", "Balance Due (LCY)", "Responsibility Center");
                    if rlCustomer.FindSet(false) then begin
                        repeat
                            xfila += 1;
                            AsignaCeldaF(xfila, 1, rlCustomer."No.", false, false, TempLExcelBuffer);
                            AsignaCeldaF(xfila, 2, rlCustomer.Name, false, false, TempLExcelBuffer);
                            AsignaCeldaF(xfila, 3, rlCustomer."Responsibility Center", false, false, TempLExcelBuffer);
                            AsignaCeldaF(xfila, 4, rlCustomer."Location Code", false, false, TempLExcelBuffer);
                            AsignaCeldaF(xfila, 5, rlCustomer."Phone No.", false, false, TempLExcelBuffer);
                            AsignaCeldaF(xfila, 6, rlCustomer.Contact, false, false, TempLExcelBuffer);
                            AsignaCeldaF(xfila, 7, Format(rlCustomer."Balance (LCY)"), false, false, TempLExcelBuffer);
                            AsignaCeldaF(xfila, 8, Format(rlCustomer."Balance Due (LCY)"), false, false, TempLExcelBuffer);
                            AsignaCeldaF(xfila, 9, Format(rlCustomer."Sales (LCY)"), false, false, TempLExcelBuffer);
                            AsignaCeldaF(xfila, 10, Format(rlCustomer."Payments (LCY)"), false, false, TempLExcelBuffer);
                        until rlCustomer.Next() = 0;
                    end;
                    TempLExcelBuffer.WriteSheet('', '', '');
                    TempLExcelBuffer.CloseBook();
                    TempLExcelBuffer.OpenExcel();
                end;
            }
            action(ATRImportarExcelC01)
            {
                ApplicationArea = All;
                Caption = 'Importar Excel';
                Image = Import;
                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    xlFicheroCliente: Text;
                    xlInStr: Instream;
                    xlHoja: Text;
                    rlCustomer: Record Customer;
                begin
                    if UploadIntoStream('Selecciona el ficehro excel para importar clientes', '', 'Ficheros Excel (*.xls)|*.xls;*.xlsx', xlFicheroCliente, xlInStr) then begin
                        xlHoja := TempLExcelBuffer.SelectSheetsNameStream(xlInStr);
                        if xlHoja = '' then begin
                            Error('Proceso Cancelado');
                        end;
                        TempLExcelBuffer.OpenBookStream(xlInStr, xlHoja);
                        TempLExcelBuffer.ReadSheet();
                        TempLExcelBuffer.SetFilter("Column No.", '<>%1&<>%2<>&%3<>&%4', 7, 8, 9, 10);
                        if TempLExcelBuffer.FindSet(false) then begin
                            repeat
                                case TempLExcelBuffer."Column No." of
                                    1:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate("No.", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    2:
                                        rlCustomer.Validate(Name, TempLExcelBuffer."Cell Value as Text");
                                    3:
                                        rlCustomer.Validate("Responsibility Center", TempLExcelBuffer."Cell Value as Text");
                                    4:
                                        rlCustomer.Validate("Location Code", TempLExcelBuffer."Cell Value as Text");
                                    5:
                                        rlCustomer.Validate("Phone No.", TempLExcelBuffer."Cell Value as Text");
                                    6:
                                        begin
                                            rlCustomer.Validate(Contact, TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Modify(true);
                                        end;
                                end;
                            until TempLExcelBuffer.Next() = 0;
                        end;
                    end else begin
                        Error('No se ha podido cargar el fichero');
                    end;
                end;
            }
            action(ATRpaginafiltrosC01)
            {
                ApplicationArea = All;
                Caption = 'Pagina de filtros';
                trigger OnAction()
                var
                    xlPaginaFiltros: FilterPageBuilder;
                    rlCustomer: Record Customer;
                    rlCustomerLedgerEntry: Record "Cust. Ledger Entry";
                    rlSalesPrice: Record "Sales Price";
                    rlVendor: Record Vendor;
                    clClientes: Label 'Clientes';
                    clMovimientos: Label 'Movimientos';
                    clPrecios: Label 'Precios';
                    pglConfiguracionC01: Page ATRTCNConfiguracionC01;
                begin
                    xlPaginaFiltros.PageCaption('Seleccione los clientes a procesar');
                    xlPaginaFiltros.AddRecord(clClientes, rlCustomer);
                    xlPaginaFiltros.AddField(clClientes, rlCustomer."No.");
                    xlPaginaFiltros.AddField(clClientes, rlCustomer.ATRTCNFechaUltimaVacunaC01);

                    xlPaginaFiltros.AddRecord(clMovimientos, rlCustomerLedgerEntry);
                    xlPaginaFiltros.AddField(clMovimientos, rlCustomerLedgerEntry."Document No.");
                    xlPaginaFiltros.AddField(clMovimientos, rlCustomerLedgerEntry."Sales (LCY)");
                    xlPaginaFiltros.AddField(clMovimientos, rlCustomerLedgerEntry."Document No.");

                    xlPaginaFiltros.AddRecord(clPrecios, rlSalesPrice);
                    xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Item No.");
                    xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Sales Type");
                    xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Starting Date");

                    if xlPaginaFiltros.RunModal() then begin
                        rlCustomer.SetView(xlPaginaFiltros.GetView(clClientes, true));
                        Message(rlCustomer.GetFilters);
                        rlVendor.SetView(xlPaginaFiltros.GetView(clClientes, true));
                        Message(rlVendor.GetFilters);
                        if rlCustomer.FindSet(false) then begin
                            repeat
                            //hacer lo que te pidan.
                            until rlCustomer.Next() = 0;
                        end;
                        Message(xlPaginaFiltros.GetView(clClientes, true));
                        Message(xlPaginaFiltros.GetView(clMovimientos, true));
                        Message(xlPaginaFiltros.GetView(clPrecios, true));
                    end else begin
                        Error('Proceso Cancelado');
                    end;

                end;
            }
        }

        addlast(History)
        {
            action(ATRTCNFrasYAbonosC01)
            {
                ApplicationArea = all;
                trigger OnAction()
                begin
                    FrasyAbonosF();
                end;
            }
        }
    }
    local procedure AsignaCeldaF(pFila: Integer; pColumna: Integer; pContenidoCelda: Text; pNegrita: Boolean; pItalica: Boolean; var TempPExcelBuffer: Record "Excel Buffer" temporary)
    begin
        TempPExcelBuffer.Init();
        TempPExcelBuffer.Validate("Row No.", pFila);
        TempPExcelBuffer.Validate("Column No.", pColumna);
        TempPExcelBuffer.Insert(true);
        TempPExcelBuffer.Validate("Cell Value as Text", pContenidoCelda);
        TempPExcelBuffer.Validate(Bold, pNegrita);
        TempPExcelBuffer.Validate(Italic, pItalica);
        TempPExcelBuffer.Modify(true);
    end;

    local procedure ExportaClienteSelecF()
    var
        rlATRTCNCabeceraPlanVacunaC01: Record ATRTCNCabeceraPlanVacunaC01;
        pglATRTCNListaCabPlanVacC01: Page ATRTCNListaCabPlanVacC01;
        rlATRTCNLinPlanVacunC01: Record ATRTCNLinPlanVacunC01;
        xInStream: InStream;
        xOutStream: OutStream;
        TempLATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        clSeparator: Label '·';
        xlNombreFichero: Text;

        rlCustomer: Record Customer;
    begin
        if pglATRTCNListaCabPlanVacC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            pglATRTCNListaCabPlanVacC01.GetSelectionFilterF(rlATRTCNCabeceraPlanVacunaC01);
            //Obtenemos el dataset
            TempLATRTCNConfiguracionC01.MiBlob.CreateOutStream(xOutStream, TextEncoding::UTF8);
            TempLATRTCNConfiguracionC01.MiBlob.CreateInStream(xInStream);
            xOutStream.WriteText(rlCustomer."No.");
            xOutStream.WriteText(clSeparator);
            xOutStream.WriteText(rlCustomer.Name);
            xOutStream.WriteText(clSeparator);
            xOutStream.WriteText(format(rlCustomer."Balance (LCY)"));
            xOutStream.WriteText(clSeparator);
            xOutStream.WriteText(rlATRTCNCabeceraPlanVacunaC01.Codigo);
        end;
    end;

    local procedure FrasyAbonosF()
    var
        TempLSalesInvoiceHeader: Record "Sales Invoice Header" temporary;
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
        rlSalesCrMemoHeader: Record "Sales Cr.Memo Header";
    begin
        rlSalesInvoiceHeader.SetRange("Sell-to Customer No.", Rec."No.");
        Message(rlSalesInvoiceHeader.GetFilter("Bill-to Customer No."));
        if rlSalesInvoiceHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesInvoiceHeader);
                TempLSalesInvoiceHeader."No." := 'F' + rlSalesInvoiceHeader."No.";
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesInvoiceHeader.Next() = 0;
        end;
        rlSalesCrMemoHeader.SetRange("Sell-to Customer No.", Rec."No.");
        Message(rlSalesCrMemoHeader.GetFilters);
        if rlSalesCrMemoHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesCrMemoHeader);
                TempLSalesInvoiceHeader."No." := 'A' + rlSalesCrMemoHeader."No.";
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesCrMemoHeader.Next() = 0;
        end;
        Page.Run(0, TempLSalesInvoiceHeader);
    end;

    var
        cuATRFuncionesAppC01: Codeunit "ATRTCNVariablesGlobalesC01";

}