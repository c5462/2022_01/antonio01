codeunit 50111 "ATRTCNVariablesGlobalesC01"
{
    SingleInstance = true;
    procedure SwitchF(pValor: Boolean)
    begin
        xSwitch := pValor;
    end;

    procedure SwitchF(): Boolean
    begin
        exit(xSwitch);
    end;

    /// <summary>
    /// Guarda el nombre a nivel global a BC
    /// </summary>
    /// <param name="pName">Text.</param>
    procedure NombreF(pName: Text)
    begin
        xName := pName;
    end;

    /// <summary>
    /// Devuelve el nombre guardado globalmente
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreF(): Text
    begin
        exit(xName);
    end;

    procedure FuncionEjemplo1F(pCodCliente: Code[20]; pParam2: Integer; var prCustomer: Record Customer) xSalida: Text
    begin

    end;

    procedure EjemploVariablesEntradaSalidaF(var pTexto: Text)
    begin
        pTexto := 'Tecon Servicios';
    end;

    procedure EjemploVariablesEntradaSalida02F(pTexto: TextBuilder)
    begin
        pTexto.Append('Tecon Servicios');
    end;

    procedure BingoF()
    var
        mlnumeros: array[20] of Text;
        i: Integer;
    begin
        Randomize(20);
        for i := 1 to ArrayLen(mlnumeros) do begin
            mlnumeros[i] := format(i);
        end;
        repeat
            //Message('La bola que ha salido es %1', mlnumeros[xlNumElementosArray]);
            mlnumeros[Random(i)] := '';
            i := CompressArray(mlnumeros);
        until CompressArray(mlnumeros) <= 0;
    end;

    procedure BingoFJJ()
    var
        mlnumeros: array[20] of Text;
        i: Integer;
        xlNum: Integer;
    begin
        for i := 1 to ArrayLen(mlnumeros) do begin
            mlnumeros[i] := format(i);
        end;
        repeat
            xlNum := Random(CompressArray(mlnumeros));
            Message(mlnumeros[xlNum]);
            mlnumeros[xlNum] := '';
        until CompressArray(mlnumeros) <= 0;
    end;

    procedure UltimoNumLineaF(pRec: Record "Sales Line"): Integer
    var
        rlRec: Record "Sales Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin //busca uno y solo el ultimo dataset
            exit(rlRec."Line No.");
        end;
    end;

    procedure UltimoNumLineaF(pRec: Record "Purchase Line"): Integer
    var
        rlRec: Record "Purchase Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin //busca uno y solo el ultimo dataset
            exit(rlRec."Line No.");
        end;
    end;

    procedure UltimoNumLineaF(pRec: Record "Gen. Journal Line"): Integer
    var
        rlRec: Record "Gen. Journal Line";
    begin
        rlRec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");
        rlRec.SetRange("Journal Template Name", pRec."Journal Template Name");
        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");
        if rlRec.FindLast() then begin //busca uno y solo el ultimo dataset
            exit(rlRec."Line No.");
        end;
    end;

    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text)
    begin
        EjemploSobrecargaF(p1, p2, p3, false);
    end;

    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text; p4: Boolean)
    var
        xlNuevoVumero: Decimal;
    begin
        p1 := 1;
        p2 := 12.45;
        p3 := 'Antonio';
        if p4 then begin
            xlNuevoVumero := p1 * p2;
        end;
    end;

    procedure UsoFuncionEjemploF()
    begin
        EjemploSobrecargaF(10, 20.4, 'ahola');
    end;

    procedure SintaxisF()
    var
        i: Integer;
        x: Integer;
        z: Integer;
        xBool: Boolean;
        rlCustomer: Record Customer;
        clSeparador: Label '·', Locked = true;
        ventana: Dialog;
        xlTexto: Text;
        xlLong: Integer;
        xlPos: Integer;
        xlCampos: Text;
        xlFecha: Date;
    begin
        //Asignacion
        //:= siempre que no sea de registro
        //Operadores
        i := x + z; //Round(x+z,0.01 redondea dos decimales, '<'Redondea al menor) serviria para redondear el decimal pero siempre siendo decimal, aunque nos de integer.
        i := 9 div 3; //division
        i := 9 mod 3; //resto
        i := Abs(9); //absoluto
        xBool := not true;

        //Operadores relacionales
        xBool := 1 < 2;
        xBool := 1 > 2;
        xBool := 1 <> 2;
        xBool := 1 <= 2;
        xBool := 1 >= 2;
        xBool := 1 = 2;
        xBool := 1 in [1, 'a', 2, 'c']; //nos devuelve true si el 1 esta dentro, sino false
        Message('El cliente %1 no tiene saldo', rlCustomer.Name);
        i := StrMenu('Enviar, Facturar, Enviar y Facturar', 3); //el tres significa que el hueco 3 se pondria como seleccionado (Enviar y Facturar)
        //una vez se selccione alguno se haria un case:
        case i of
            0:
                Error('Le has dao a cancelar bobo');
            1:
                ; //enviar
            2:
                ;   //facturar
            3:
                ; //Enviar y Facturar
        end;
        //VENTANAS
        if GuiAllowed then begin //siempre que se haya notificaciones que se muestre al usuario se pone el if con el guidAllowed
            ventana.Open('Procesando bucle #1##########');
        end;
        for i := 1 to 1000 do begin
            if GuiAllowed then begin
                ventana.Update(1, i);
            end;
        end;
        if GuiAllowed then begin
            ventana.Close();
        end;

        //Fucniones de cadenas de texto
        xlLong := MaxStrLen(xlTexto);
        xlLong := MaxStrLen('ABC');
        xlTexto := CopyStr('ABC', 2, 1); //B
        xlTexto := CopyStr('ABC', 2); //BC
        xlTexto := StrSubstNo('ABC', 2, 1); //igual que copystr
        xlPos := StrPos('Elefante', 'e'); //devuelve la pos donde esta, sin distinguir en Mayus, 3
        xlLong := StrLen('Elefante');//Devuelve 4 sin contar espacios vacios etc...

        xlTexto := xlTexto.ToUpper();
        xlTexto := xlTexto.ToLower();
        xlTexto := xlTexto.TrimStart('\').TrimEnd('\').Trim() + '\';

        xlTexto := '123,456,789';
        Message(SelectStr(2, xlTexto)); //456
        xlTexto := '123·aaaaa,456·789';
        Message(xlTexto.Split(clSeparador).Get(3)); //789
        //foreach xlTexto in xlCampos do begin end;


        xlTexto := '21-05-2022';
        Evaluate(xlFecha, ConvertStr(xlTexto, '-', '/')); //convierte la fecha anterior con sus separadores para BC.
        Evaluate(xlFecha, xlTexto.Replace('-', '/')); //igual que ConvertStr pero mas efectivo.
        xlTexto := 'Elefante';
        Message(DelChr(xlTexto, '<', 'E')); //me devuelve lefante
        Message(DelChr(xlTexto, '<=>', 'E')); //me devuelve lfant, quita todas las E

        xlPos := Power(3, 2);
        xlTexto := UserId();
        xlTexto := CompanyName();


        //CUANDO SE UTILIZA FIND
        rlCustomer.SetFilter(Name, '%1|%2', '*A*', '*B*');
        if rlCustomer.FindSet(true, true) then begin
            repeat
            until rlCustomer.Next() = 0;
        end;
        rlCustomer.Find();
    end;

    procedure LeftF(pCadena: Text; pNumCaracteres: Integer): Text
    begin
        exit(CopyStr(pCadena, 1, pNumCaracteres));
    end;

    procedure RightF(pCadena: Text; pNumCaracteres: Integer): Text
    begin
        exit(CopyStr(pCadena, StrLen(pCadena) - pNumCaracteres + 1, pNumCaracteres));
    end;

    procedure streamsF()
    var
        xInStream: InStream;
        xOutStream: OutStream;
        TempLATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        TempLATRTCNConfiguracionC01.MiBlob.CreateOutStream(xOutStream);
        xOutStream.WriteText('ATR');
        TempLATRTCNConfiguracionC01.MiBlob.CreateInStream(xInStream);
        xInStream.ReadText(xlLinea);
        xlNombreFichero := 'Pedido.csv';
        if not DownloadFromStream(xInStream, '', '', '', xlNombreFichero) then begin
            Error('Fichero no descargado');
        end;
    end;

    procedure RecuperarClienteF()
    var
        rlCustomer: Record Customer;
    begin
        rlCustomer.Get('10000');
    end;

    procedure DimensionCodeF()
    var
        //el GET() no respeta filtros
        rlSalesLine: Record "Sales Line";
        rlDimensionSetEntry: Record "Dimension Set Entry";
        rlCompanyInformation: Record "Company Information";
    begin
        rlSalesLine.Get(rlSalesLine."Document Type"::Order, rlSalesLine."Document No.", rlSalesLine."Line No.");
        rlDimensionSetEntry.Get(7, 'GRUPONEGOCIO');
        rlCompanyInformation.Get();
    end;

    local procedure OtrasFuncionesF()
    var
        rlSalesHeader: Record "Sales Header";
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        rlSalesHeader.TransferFields(rlSalesInvoiceHeader);
    end;

    local procedure UsoDeListasF()
    var
        xlLista: List of [Code[20]];
    begin
    end;

    procedure AbrirListaClientesF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;

        clNombreDato: Label 'CodigoCliente';
    begin
        if pNotificacion.HasData(clNombreDato) then begin
            rlCustomer.SetRange("No.", pNotificacion.GetData(clNombreDato));
        end;
        Page.Run(page::"Customer List", rlCustomer);
    end;

    procedure AbrirFichaClienteF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;

        clNombreDato: Label 'CodigoCliente';
    begin
        if pNotificacion.HasData(clNombreDato) then begin
            rlCustomer.Get(pNotificacion.GetData(clNombreDato));
        end;
        Page.Run(page::"Customer Card", rlCustomer);
    end;

    procedure CodeunitSalesPostOnAfterPostSalesDocF(var SalesHeader: Record "Sales Header")
    var
        rlCustomer: Record Customer;
        xlNotificacion: Notification;
        rlSalesLine: Record "Sales Line";
    begin
        //Verificar que el cliente tuene vcuna y dar ntoificacion con su ficha
        if rlCustomer.Get(SalesHeader."Sell-to Customer No.") then begin
            if rlCustomer.ATRTCNTipoVacunaC01 = '' then begin
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message('El cliente no tiene ninguna vacuna');
                xlNotificacion.AddAction('Abrir ficha cliente', Codeunit::ATRTCNVariablesGlobalesC01, 'AbrirFichaClienteF');
                xlNotificacion.SetData('CodigoCliente', rlCustomer."No.");
                xlNotificacion.Send()
            end;
        end;
        //Verificar que las lineas de pedido sea >1000
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
        rlSalesLine.SetFilter(Amount, '<%1', 100);
        if rlSalesLine.FindSet(false) then begin
            repeat
                Clear(xlNotificacion);
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo('La linea %1 del documento %2 nº %3 con importe %4 es menor de 100', rlSalesLine."Line No.", SalesHeader."Document Type", rlSalesLine."Document No.", rlSalesLine.Amount));
                xlNotificacion.Send();
            until rlSalesLine.Next() = 0;
        end;
    end;

    procedure ImportandoPedidosF(pImportando: Boolean)
    begin
        xImportandoPedidos := pImportando;
    end;

    procedure ImportandoPedidosF(): Boolean
    begin
        exit(xImportandoPedidos);
    end;

    procedure CumpleFiltroF(pCodigo: code[20]; pFiltro: Text): Boolean
    var
        TempLCustomer: Record Customer temporary;
    begin
        TempLCustomer.Init();
        TempLCustomer."No." := pCodigo;
        TempLCustomer.Insert(false);
        TempLCustomer.SetFilter("No.", pFiltro);
        exit(not TempLCustomer.IsEmpty);
    end;

    var
        xSwitch: Boolean;
        xName: Text;
        xlTexto: Text;
        xImportandoPedidos: Boolean;
}