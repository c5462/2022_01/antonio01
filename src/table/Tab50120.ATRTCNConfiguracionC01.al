table 50120 "ATRTCNConfiguracionC01"
{
    Caption = 'Configuración de la app';
    DataClassification = OrganizationIdentifiableInformation;
    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id. de registro';
            DataClassification = SystemMetadata;
        }
        field(2; CodCteWeb; Code[20])
        {
            Caption = 'Código cliente para web';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
            //TestTableRelation = true; (OBSOLETO/NO SE USA)
            ValidateTableRelation = true;
        }
        field(3; TextoRegistro; Text[250])
        {
            Caption = 'Texto Registro';
            DataClassification = SystemMetadata;
        }
        field(4; TipoDoc; Enum ATRATTipoDocC01)
        {
            Caption = 'Tipo documento';
            DataClassification = SystemMetadata;
        }

        field(5; NombreCliente; Text[100])
        {
            Caption = 'Nombre cliente';
            FieldClass = FlowField;
            CalcFormula = lookup(Customer.Name where("No." = field(CodCteWeb)));
            Editable = false;
        }

        field(6; TipoTabla; Enum ATRTCNTipoTablaC01)
        {
            Caption = 'Tipo de tabla';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            begin
                if (xRec.TipoTabla <> Rec.TipoTabla) then begin
                    Rec.Validate(CodTabla, '');
                end;
            end;
        }


        field(7; CodTabla; Code[20])
        {
            Caption = 'Codigo de tabla';
            DataClassification = SystemMetadata;
            TableRelation = if (TipoTabla = const(Cliente)) Customer
            else
            if (TipoTabla = const(Contacto)) Contact
            else
            if (TipoTabla = const(Empleado)) Employee
            else
            if (TipoTabla = const(Proveedor)) Vendor
            else
            if (TipoTabla = const(Recurso)) Resource;
        }

        field(8; ColorFondo; Enum ATRTCNColoresC01)
        {
            Caption = 'Colores del fondo';
            DataClassification = SystemMetadata;
        }
        field(9; ColorLetra; Enum ATRTCNColoresC01)
        {
            Caption = 'Colores de letra';
            DataClassification = SystemMetadata;
        }
        field(10; UnidadesDisponibles; Decimal)
        {
            Caption = 'Unidades disponibles';
            FieldClass = FlowField;
            CalcFormula = sum("Item Ledger Entry".Quantity where("Item No." = field(FiltroProducto), "Posting Date" = field(FechaRegistro), "Entry Type" = field(TipoMovimiento)));
        }
        field(11; FiltroProducto; Code[20])
        {
            TableRelation = Item."No.";
            Caption = 'Filtro Producto';
            FieldClass = FlowFilter;
        }
        field(12; FechaRegistro; Date)
        {
            Caption = 'Filtro Fecha Registro';
            FieldClass = FlowFilter;
        }
        field(13; TipoMovimiento; Enum "Item Ledger Entry Type")
        {
            Caption = 'Filtro Tipo Movimiento';
            FieldClass = FlowFilter;
        }
        field(14; IdPassw; Guid)
        {
            DataClassification = SystemMetadata;
        }
        field(15; MiBlob; Blob)
        {
            DataClassification = SystemMetadata;
        }


    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
    }
    /// <summary>
    /// Recupera el registro de la tabla actual y sino existe, lo crea.
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure Getf(): Boolean
    begin
        if not Rec.Get() then begin
            Rec.Init();
            Rec.Insert(true)
        end;
        exit(true);
    end;
    /// <summary>
    /// Devuelve el nombre del cliente pasado en el parametro pCodCliente. Sino existe devuelve un texto vacio.
    /// </summary>
    /// <param name="pCodCliente"></param>
    /// <returns></returns>
    procedure NombreClientef(pCodCliente: Code[20]): Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(pCodCliente) then begin
            exit(rlCustomer.Name);
        end;
    end;

    procedure NombreTablaf(pTipoTabla: Enum ATRTCNTipoTablaC01; pCodTabla: Code[20]): Text
    var
        rlContact: Record Contact;
        rlEmployee: Record Employee;
        rlVendor: Record Vendor;
        rlResource: Record Resource;
        rlATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01;
    begin
        case pTipoTabla of
            pTipoTabla::Cliente:
                begin
                    exit(rlATRTCNConfiguracionC01.NombreClientef(pCodTabla));
                end;
            pTipoTabla::Contacto:
                begin
                    if rlContact.Get(pCodTabla) then begin
                        exit(rlContact.Name);
                    end;
                end;
            pTipoTabla::Empleado:
                begin
                    if rlEmployee.Get(pCodTabla) then begin
                        exit(rlEmployee.FullName());
                    end;
                end;
            pTipoTabla::Proveedor:
                begin
                    if rlVendor.Get(pCodTabla) then begin
                        exit(rlVendor.Name);
                    end;
                end;
            pTipoTabla::Recurso:
                begin
                    if rlResource.Get(pCodTabla) then begin
                        exit(rlResource.Name);
                    end;
                end;
        end;
    end;

    [NonDebuggable]
    procedure PasswordF(pPassword: Text)
    begin
        Rec.Validate(IdPassw, CreateGuid());
        Rec.Modify(true);
        IsolatedStorage.Set(Rec.IdPassw, pPassword, DataScope::Company);
    end;

    procedure PasswordF() xSalida: Text
    begin
        if not IsolatedStorage.Get(Rec.IdPassw, DataScope::Company, xSalida) then begin
            Error('NO SE ENCUENTRAS PSW');
        end;
    end;
}