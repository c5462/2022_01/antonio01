pageextension 50116 "ATRTCNSalesOrderListExtC01" extends "Sales Order List"
{
    actions
    {
        addlast(processing)
        {
            action(ATRExportarACsvC01)
            {
                ApplicationArea = all;
                Caption = 'Exportar Pedidos Venta';
                trigger OnAction()
                begin
                    ExportarPedidosVentaF();
                end;
            }
            action(ATRImportarDesdeCsvC01)
            {
                ApplicationArea = all;
                Caption = 'Importar Pedidos Venta';
                trigger OnAction()
                begin
                    ImportarPedidosVentaF();
                end;
            }
        }
    }

    procedure ExportarPedidosVentaF()
    var

        pglSalesOrderList: Page "Sales Order List";
        xlOutStr: OutStream;
        TempLATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        rlATRTCNLinPlanVacunacionC01: Record "Sales Line";
        rlSalesHeader: Record "Sales Header";
        xlNombreFichero: Text;
        xlInStr: Instream;
        xlLinea: Text;
        culTypeHelper: Codeunit "Type Helper";
        xlSeparador: Label '·', Locked = true;
    begin
        CurrPage.SetSelectionFilter(rlSalesHeader);
        //setLoadFields(Campos a exportar);
        if rlSalesHeader.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
            TempLATRTCNConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
            repeat
                xlOutStr.WriteText('C');
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."No.");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Document Type", 0, 9));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Bill-to Name");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Bill-to Contact");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Bill-to Customer No.");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Sell-to Customer No.");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Status", 0, 9));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Posting Date", 0, 9));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Due Date", 0, 9));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Shipment Date", 0, 9));
                xlOutStr.WriteText();
                //RECORREMOS LAS LÍNEAS
                rlATRTCNLinPlanVacunacionC01.SetRange("Document No.", rlSalesHeader."No.");
                rlATRTCNLinPlanVacunacionC01.SetRange("Document Type", rlSalesHeader."Document Type");
                //setLoadFields(Campos a exportar);
                if rlATRTCNLinPlanVacunacionC01.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
                    //1 p: campo por el que queremos filtrar el parametro. 2p: "DESDE" | VALOR, si no pongo el 3, estoy diciendo que filtrame el campo 1 con este VALOR. Si le ponemos el 3p: "HASTA".
                    //SETFILTER. 3 valores. Primero: Campo. Segundo: Cadena. Tecero: Valores.
                    repeat
                        xlOutStr.WriteText('L');
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlATRTCNLinPlanVacunacionC01."Line No.", 0, 9));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlATRTCNLinPlanVacunacionC01."Document No.", 0, 9));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlATRTCNLinPlanVacunacionC01."Document Type", 0, 9));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlATRTCNLinPlanVacunacionC01."Type", 0, 9));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(rlATRTCNLinPlanVacunacionC01."No.");
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(rlATRTCNLinPlanVacunacionC01.Description);
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlATRTCNLinPlanVacunacionC01.Amount, 0, 9));
                        xlOutStr.WriteText()
                    until rlATRTCNLinPlanVacunacionC01.Next() = 0;
                end;
            until rlSalesHeader.Next() = 0;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        xlNombreFichero := 'PedidosVentas.csv';
        TempLATRTCNConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlInStr.ReadText(xlLinea);
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('Error de descarga del fichero %1', xlNombreFichero);
        end;

    end;

    [TryFunction]
    local procedure ImportarPedidosVentaF()
    var
        xlInStr: Instream;
        xlLinea: Text;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        TempLATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        clSeparador: Label '·', Locked = true;
        xlFecha: Date;
        xlLNum: Integer;
        xlTipo: Enum "Sales Document Type";
        xlAmount: Decimal;
        xltipoL: Enum "Sales Line Type";
        xlStatus: Enum "Sales Document Status";
        i: Integer;
        cdLATRTCNCapturaErroresC01: Codeunit ATRTCNCapturaErroresC01;
        rlATRTCNLogC01: Record ATRTCNLogC01;
        xlTextoError: Text;
    begin
        TempLATRTCNConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin //EOS END OF STRING. LEE HASTA EL FINAL.
                xlInStr.ReadText(xlLinea);
                //Insertar Cabecera o líneas en funcion del primer caracter de la línea
                case true of
                    xlLinea[1] = 'C':
                        //DAR DE ALTA CABECERA
                        begin
                            rlSalesHeader.Init();
                            if Evaluate(xlTipo, xlLinea.Split(clSeparador).Get(3)) then begin
                                rlSalesHeader.Validate("Document Type", xlTipo);
                            end;
                            rlSalesHeader.Validate("No.", xlLinea.Split(clSeparador).Get(2));
                            rlSalesHeader.Insert(true);
                            for i := 4 to 11 do begin
                                if not cdLATRTCNCapturaErroresC01.ErroresImportarPedidosVentaF(rlSalesHeader, clSeparador, i, xlLinea) then begin
                                    rlATRTCNLogC01.Validate(Id, CreateGuid());
                                    xlTextoError := 'El campo no se ha podido insertar';
                                    rlATRTCNLogC01.Validate(Mensaje, GetLastErrorText());
                                end;
                            end;
                            rlSalesHeader.Modify(true);
                        end;
                    xlLinea[1] = 'L':
                        begin
                            rlSalesLine.Init();
                            if Evaluate(xlTipo, xlLinea.Split(clSeparador).Get(4)) then begin
                                rlSalesLine.Validate("Document Type", xlTipo);
                            end;
                            rlSalesLine.Validate("Document No.", xlLinea.Split(clSeparador).Get(3));
                            if Evaluate(xlLNum, xlLinea.Split(clSeparador).Get(2)) then begin //evluate evalua que lo que te pasan sea del tipo que espera.
                                rlSalesLine.Validate("Line No.", xlLNum);
                            end;
                            rlSalesLine.Insert(true);
                            for i := 5 to 8 do begin
                                if not cdLATRTCNCapturaErroresC01.ErroresImportarPedidosVentaF(rlSalesLine, clSeparador, i, xlLinea) then begin
                                    rlATRTCNLogC01.Validate(Id, CreateGuid());
                                    xlTextoError := 'El campo no se ha podido insertar';
                                    rlATRTCNLogC01.Validate(Mensaje, GetLastErrorText());
                                end;
                            end;
                            rlSalesLine.Modify(true);

                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor. Error (%1', GetLastErrorText());
        end;
    end;
}

