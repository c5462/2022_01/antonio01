pageextension 50110 "ATRTCNItemListExtC01" extends "Item List"
{

    actions
    {

        addafter(SalesPriceListsDiscounts)
        {
            action(ATRTCNAbrirListaPaqueteC01)
            {
                Caption = 'Abrir Lista de Paquetes';
                Image = open;
                RunObject = page ATRTCNListaPaquetesC01;
                RunPageLink = CodProducto = field("No.");
                ApplicationArea = All;
                Promoted = true;
            }
        }
    }
}
