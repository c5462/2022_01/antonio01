page 50121 "ATRTCNEntradaDeDatosC01"
{
    Caption = 'Entrada de datos varios';
    PageType = StandardDialog;
    SaveValues = true;

    layout
    {
        area(content)
        {
            field(texto01; mtextos[1])
            {
                ApplicationArea = All;
                Visible = xTxtVisible01;
                CaptionClass = mTxtCaptionClass[1];
            }
            field(texto02; mtextos[2])
            {
                ApplicationArea = All;
                Visible = xTxtVisible02;
                CaptionClass = mTxtCaptionClass[2];
            }
            field(texto03; mtextos[3])
            {
                ApplicationArea = All;
                Visible = xTxtVisible03;
                CaptionClass = mTxtCaptionClass[3];
            }
            field(texto04; mtextos[4])
            {
                ApplicationArea = All;
                Visible = xTxtVisible04;
                CaptionClass = mTxtCaptionClass[4];
            }
            field(boolean01; mboolean[1])
            {
                ApplicationArea = All;
                Visible = xboolVisible04;
                CaptionClass = mBoolCaptionClass[1];
            }
            field(boolean02; mboolean[2])
            {
                ApplicationArea = All;
                Visible = xboolVisible05;
                CaptionClass = mBoolCaptionClass[2];
            }
            field(boolean03; mboolean[3])
            {
                ApplicationArea = All;
                Visible = xboolVisible06;
                CaptionClass = mBoolCaptionClass[3];
            }
            field(decimal01; mdecimal[1])
            {
                ApplicationArea = All;
                Visible = xDecimalVisible01;
                CaptionClass = mDecimalCaptionClass[1];
            }
            field(decimal02; mdecimal[2])
            {
                ApplicationArea = All;
                Visible = xDecimalVisible02;
                CaptionClass = mDecimalCaptionClass[2];
            }
            field(decimal03; mdecimal[3])
            {
                ApplicationArea = All;
                Visible = xDecimalVisible03;
                CaptionClass = mDecimalCaptionClass[3];
            }
            field(Pass01; mPasss[1])
            {
                ApplicationArea = All;
                Visible = xPassVisible01;
                CaptionClass = mPassCaptionClass[1];
                ExtendedDatatype = Masked;
            }
            field(Pass02; mPasss[2])
            {
                ApplicationArea = All;
                Visible = xPassVisible02;
                CaptionClass = mPassCaptionClass[2];
                ExtendedDatatype = Masked;
            }
            field(Pass03; mPasss[3])
            {
                ApplicationArea = All;
                Visible = xPassVisible03;
                CaptionClass = mPassCaptionClass[3];
                ExtendedDatatype = Masked;
            }
        }
    }
    var
        mtextos: array[10] of Text;
        xTxtVisible01: Boolean;
        xTxtVisible02: Boolean;
        xTxtVisible03: Boolean;
        mTxtCaptionClass: array[10] of Text;
        xPunteroTextosPasados: Integer;
        xPunteroTextosLeidos: Integer;
        mboolean: Array[10] of Boolean;
        xTxtVisible04: Boolean;
        xboolVisible04: Boolean;
        xboolVisible05: Boolean;
        xboolVisible06: Boolean;
        mBoolCaptionClass: Array[10] of Text;

        /// <summary>
        /// Hace que se muestre un campo tipo texto con el caption y valor pasados por parametros.
        /// </summary>
        xPunteroBoolsPasados: Integer;
        xPunteroBoolsLeidos: Integer;
        mdecimal: Array[10] of Decimal;
        mDecimalCaptionClass: Array[10] of Text;
        xDecimalVisible01: Boolean;
        xDecimalVisible02: Boolean;
        xDecimalVisible03: Boolean;
        xPunteroDecimalPasados: Integer;
        xPunteroDecimalLeidos: Integer;
        mPasss: Array[10] of Text;
        xPassVisible01: Boolean;
        xPassVisible02: Boolean;
        xPassVisible03: Boolean;
        mPassCaptionClass: Array[10] of Text;
        xPunteroPassPasados: Integer;
        xPunteroPassLeidos: Integer;
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text)
    begin
        xPunteroTextosPasados += 1;
        mTxtCaptionClass[xPunteroTextosPasados] := pCaption;
        mtextos[xPunteroTextosPasados] := pValorInicial;
        xTxtVisible01 := xPunteroTextosPasados >= 1;
        xTxtVisible02 := xPunteroTextosPasados >= 2;
        xTxtVisible03 := xPunteroTextosPasados >= 3;
        xTxtVisible04 := xPunteroTextosPasados >= 4;
    end;
    /// <summary>
    /// Hace que se muestre un campo tipo booleano con el caption y valor pasados por parametros.
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Boolean)
    begin
        xPunteroBoolsPasados += 1;
        mBoolCaptionClass[xPunteroBoolsPasados] := pCaption;
        mboolean[xPunteroBoolsPasados] := pValorInicial;
        xboolVisible04 := xPunteroBoolsPasados >= 1;
        xboolVisible05 := xPunteroBoolsPasados >= 2;
        xboolVisible06 := xPunteroBoolsPasados >= 3;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: Decimal)
    begin
        xPunteroDecimalPasados += 1;
        mDecimalCaptionClass[xPunteroDecimalPasados] := pCaption;
        mDecimal[xPunteroDecimalPasados] := pValorInicial;
        xDecimalVisible01 := xPunteroDecimalPasados >= 1;
        xDecimalVisible02 := xPunteroDecimalPasados >= 2;
        xDecimalVisible03 := xPunteroDecimalPasados >= 3;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: Text; pPass: Boolean)
    begin
        if pPass then begin
            xPunteroPassPasados += 1;
            mPassCaptionClass[xPunteroPassPasados] := pCaption;
            mPasss[xPunteroPassPasados] := pValorInicial;
            xPassVisible01 := xPunteroPassPasados >= 1;
            xPassVisible02 := xPunteroPassPasados >= 2;
            xPassVisible03 := xPunteroPassPasados >= 3;
        end else begin
            CampoF(pCaption, pValorInicial);
        end;
    end;


    //SERIA VALIDA TAMBIEN PERO FALTARIA LA CONDICION DEL IF PREGUNTANDO EL BLANCO
    // procedure Campo02F(pCaption: Text; pValorInicial: Text)
    // var
    //     i: Integer;
    // begin
    //     for i := 1 to ArrayLen(xTxCaptionClass02) do begin
    //         xTxCaptionClass02[i] := pCaption;
    //         xTexto02[i] := pValorInicial;
    //         xTxtVisible02 := true;
    //     end;
    // end;


    /// <summary>
    /// Devuelve el texto introducido por el usario.
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(): Text
    begin
        xPunteroTextosLeidos += 1;
        exit(mtextos[xPunteroTextosLeidos]);
    end;

    procedure CampoF(var pSalida: Decimal): Decimal
    begin
        xPunteroDecimalLeidos += 1;
        pSalida := mdecimal[xPunteroDecimalLeidos];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Boolean): Boolean
    begin
        xPunteroBoolsLeidos += 1;
        pSalida := mboolean[xPunteroBoolsLeidos];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Text): Text
    begin
        xPunteroPassLeidos += 1;
        pSalida := mPasss[xPunteroPassLeidos];
        exit(pSalida);
    end;

    procedure CampoF(var xPuntero: Integer): Text
    begin
        //xPunteroTextosLeidos += 1;
        exit(mtextos[xPuntero]);
    end;

    procedure CampoF(var pSalida: Decimal; var xPuntero: Integer): Decimal
    begin
        //xPunteroDecimalLeidos += 1;
        pSalida := mdecimal[xPuntero];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Boolean; var xPuntero: Integer): Boolean
    begin
        //xPunteroBoolsLeidos += 1;
        pSalida := mboolean[xPuntero];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Text; var xPuntero: Integer): Text
    begin
        //xPunteroPassLeidos += 1;
        pSalida := mPasss[xPuntero];
        exit(pSalida);
    end;
}
