table 50114 "ATRTCNLinPlanVacunC01"
{
    Caption = 'Linea Planificacion Vacunacion';
    DataClassification = ToBeClassified;
    DrillDownPageId = ATRTCNSubPagLinPlanVacC01;

    fields
    {
        field(1; Codigo; Code[20])
        {
            Caption = 'Codigo';
            DataClassification = SystemMetadata;
        }
        field(2; "NºLinea"; Integer)
        {
            Caption = 'N ºLinea';
            DataClassification = SystemMetadata;
        }
        field(3; ClienteAVacunar; Code[20])
        {
            TableRelation = Customer."No.";
            Caption = 'Cliente A Vacunar';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            var
                rlATRTCNCabeceraPlanVacunaC01: Record ATRTCNCabeceraPlanVacunaC01;
            begin
                if (rec.FechaVacunacion = 0D) then begin //lo de D siempre MAYUSCULAS
                    if (rlATRTCNCabeceraPlanVacunaC01.Get(rec.Codigo)) then begin
                        rec.Validate(FechaVacunacion, rlATRTCNCabeceraPlanVacunaC01."FechaIncioVacu.Planif.");
                    end;
                end;
            end;
        }
        field(4; FechaVacunacion; Date)
        {
            Caption = 'Fecha Vacunacion';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            begin
                calcularFechaProxVacunacionF();
            end;
        }
        field(5; CodigoVacuna; Code[20])
        {
            Caption = 'Tipo Vacuna';
            DataClassification = SystemMetadata;
            TableRelation = ATRTCNVariosC01.Codigo where(TipoVacuna = const(Vacuna), Bloqueado = const(false));
            trigger OnValidate()
            begin
                calcularFechaProxVacunacionF();
            end;
        }
        field(6; CodigoOtros; Code[20])
        {
            Caption = 'Tipo Otros';
            DataClassification = SystemMetadata;
            TableRelation = ATRTCNVariosC01.Codigo where(TipoVacuna = const(Otros), Bloqueado = const(false));
        }
        field(7; FechaSiguienteVacunacion; Date)
        {
            Caption = 'Fecha de siguiente vacunacion';
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; Codigo, "NºLinea")
        {
            Clustered = true;
        }
    }
    procedure NombreClienteF(): Text
    Var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(Rec.Codigo) then begin
            exit(rlCustomer.Name);
        end;
    end;

    procedure DescripcionOtrosF(pTipo: Enum ATRTCNTipoVacunaC01; pCodigo: Code[20]): Text
    var
        rlATRTCNVariosC01: Record ATRTCNVariosC01;
    begin
        if rlATRTCNVariosC01.Get(pTipo, pCodigo) then begin
            exit(rlATRTCNVariosC01.Descripcion);
        end;
    end;

    procedure calcularFechaProxVacunacionF()
    var
        rlATRTCNVariosC01: Record ATRTCNVariosC01;
    begin
        if (Rec.FechaVacunacion <> 0D) then begin
            if (rlATRTCNVariosC01.Get(rlATRTCNVariosC01.TipoVacuna::Vacuna, Rec.Codigo)) then begin
                Rec.Validate(FechaSiguienteVacunacion, CalcDate(rlATRTCNVariosC01.PeriodoSigVacunacion, Rec.FechaVacunacion));
            end;
        end;
    end;
}
