/// <summary>
/// Codeunit ATRTCNCapturaErroresC01 (ID 50114).
/// </summary>
codeunit 50114 "ATRTCNCapturaErroresC01"
{
    trigger OnRun()
    begin

    end;

    /// <summary>
    /// ErroresImportarPedidosVentaF.
    /// </summary>
    /// <param name="pSalesHeader">Record "Sales Header".</param>
    /// <param name="pClSeparador">Text.</param>
    /// <param name="PPosicion">Integer.</param>
    [TryFunction]
    procedure ErroresImportarPedidosVentaF(pSalesHeader: Record "Sales Header"; pClSeparador: Text; PPosicion: Integer; pLinea: Text)
    var
        xlStatus: Enum "Sales Document Status";
        xlFecha: Date;
    begin
        case PPosicion of
            7:
                pSalesHeader.Validate("Sell-to Customer No.", pLinea.Split(pClSeparador).Get(7));
            6:
                pSalesHeader.Validate("Bill-to Customer No.", pLinea.Split(pClSeparador).Get(6));
            4:
                pSalesHeader.Validate("Bill-to Name", pLinea.Split(pClSeparador).Get(4));
            5:
                pSalesHeader.Validate("Bill-to Contact", pLinea.Split(pClSeparador).Get(5));
            8:
                if Evaluate(xlStatus, pLinea.Split(pClSeparador).Get(8)) then begin
                    pSalesHeader.Validate("Status", xlStatus);
                end;
            9:
                if Evaluate(xlFecha, pLinea.Split(pClSeparador).Get(9)) then begin
                    pSalesHeader.Validate("Posting Date", xlFecha);
                end;
            10:
                if Evaluate(xlFecha, pLinea.Split(pClSeparador).Get(10)) then begin
                    pSalesHeader.Validate("Due Date", xlFecha);
                end;
            11:
                if Evaluate(xlFecha, pLinea.Split(pClSeparador).Get(11)) then begin
                    pSalesHeader.Validate("Shipment Date", xlFecha);
                end;
        end;
    end;

    /// <summary>
    /// ErroresImportarPedidosVentaF.
    /// </summary>
    /// <param name="pSalesLine">Record "Sales Line".</param>
    /// <param name="pClSeparador">Text.</param>
    /// <param name="PPosicion">Integer.</param>
    [TryFunction]
    procedure ErroresImportarPedidosVentaF(pSalesLine: Record "Sales Line"; pClSeparador: Text; PPosicion: Integer; pLinea: Text)
    var
        xltipoL: Enum "Sales Line Type";
        xlAmount: Decimal;
    begin
        case PPosicion of
            5:
                if Evaluate(xltipoL, pLinea.Split(pClSeparador).Get(5)) then begin
                    pSalesLine.Validate(Type, xltipoL);
                end;
            6:
                pSalesLine.Validate("No.", pLinea.Split(pClSeparador).Get(6));
            7:
                pSalesLine.Validate(Description, pLinea.Split(pClSeparador).Get(7));
            8:
                if Evaluate(xlAmount, pLinea.Split(pClSeparador).Get(8)) then begin
                    pSalesLine.Validate(Amount, xlAmount);
                end;
        end
    end;
}