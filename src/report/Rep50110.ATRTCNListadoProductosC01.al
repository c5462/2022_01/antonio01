report 50110 "ATRTCNListadoProductosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'Listadoproductos.rdl';
    AdditionalSearchTerms = 'Listado productos Ventas/Compras';
    Caption = 'Listado productos Ventas/Compras';

    dataset
    {
        dataitem(Productos; Item)
        {
            RequestFilterFields = "No.", "Item Category Code";
            column(No_Productos; "No.")
            {
            }
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }
            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                RequestFilterFields = "Posting Date";
                column(Document_No_Ventas; "Document No.")
                {
                }
                //DataItemLink = "No." = field("No."); igual que el trigger OnDataItem
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }

                column(Quantity_FacturaVenta; Quantity)
                {
                }
                column(NombreCliente; rCustomer.Name)
                {
                }
                column(NoCliente; rCustomer."No.")
                {
                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }
                trigger OnPreDataItem()
                begin
                    FacturaVenta.SetRange("No.", Productos."No.");
                end;

                trigger OnAfterGetRecord()
                begin
                    if not rCustomer.Get(FacturaVenta."Sell-to Customer No.") then begin
                        Clear(rCustomer);
                    end;
                    // rCustomer.SetRange("No.", FacturaVenta."Sell-to Customer No.");
                    // rCustomer.FindFirst();
                end;
            }
            dataitem(FacturaCompra; "Purch. Inv. Line")
            {
                RequestFilterFields = "Posting Date";
                column(Document_No_Compras; "Document No.")
                {

                }
                column(Quantity_FacturaCompra; Quantity)
                {
                }
                column(UnitCost_FacturaCompra; "Unit Cost")
                {
                }
                column(AmountIncludingVAT_FacturaCompra; "Amount Including VAT")
                {
                }
                column(NombreProveedor; rVendor.Name)
                {
                }
                column(NoProveedor; rVendor."No.")
                {
                }
                column(PostingDate_FacturaCompra; "Posting Date")
                {
                }
                trigger OnPreDataItem()
                begin
                    FacturaCompra.SetRange("No.", Productos."No.");
                end;

                trigger OnAfterGetRecord()
                begin
                    if not rVendor.Get(FacturaCompra."Buy-from Vendor No.") then begin
                        Clear(rVendor);
                    end;
                    // rCustomer.SetRange("No.", FacturaVenta."Sell-to Customer No.");
                    // rCustomer.FindFirst();
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                // action(ActionName)
                // {
                //     ApplicationArea = All;

                // }
            }
        }
    }

    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;
}