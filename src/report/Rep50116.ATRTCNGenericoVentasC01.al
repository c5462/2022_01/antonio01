report 50116 "ATRTCNGenericoVentasC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'genericoventas.rdl';
    DefaultLayout = RDLC;

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            RequestFilterFields = "No.";
            dataitem("Sales Invoice Line"; "Sales Invoice Line")
            {
                DataItemLink = "Document No." = field("No.");
                trigger OnAfterGetRecord()
                var
                    myInt: Integer;
                begin
                    ATRTCN_LinGenericaVentasC01.Init();
                    ATRTCN_LinGenericaVentasC01.TransferFields("Sales Invoice Line");
                    ATRTCN_LinGenericaVentasC01.Insert(false);
                end;
            }

            trigger OnPreDataItem()
            var
                myInt: Integer;
            begin
                if "Sales Invoice Header".GetFilters = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            var
                myInt: Integer;
            begin

                TCN_CabeceraGenericaVentas.Init();
                TCN_CabeceraGenericaVentas.TransferFields("Sales Invoice Header");
                TCN_CabeceraGenericaVentas.Insert(false);
                xCaptionInforme := 'Factura';
            end;
        }
        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            RequestFilterFields = "No.";
            dataitem("Sales Shipment Line"; "Sales Shipment Line")
            {
                DataItemLink = "Document No." = field("No.");
                trigger OnAfterGetRecord()
                var
                    myInt: Integer;
                begin
                    ATRTCN_LinGenericaVentasC01.Init();
                    ATRTCN_LinGenericaVentasC01.TransferFields("Sales Shipment Line");
                    ATRTCN_LinGenericaVentasC01.Insert(false);
                end;
            }

            trigger OnPreDataItem()
            var
                myInt: Integer;
            begin
                if "Sales Shipment Header".GetFilters = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            var
                myInt: Integer;
            begin
                TCN_CabeceraGenericaVentas.Init();
                TCN_CabeceraGenericaVentas.TransferFields("Sales Shipment Header");
                TCN_CabeceraGenericaVentas.Insert(false);
                xCaptionInforme := 'Albarán';
            end;
        }
        dataitem(TCN_CabeceraGenericaVentas; TCN_CabeceraGenericaVentas)
        {
            RequestFilterFields = "No.";
            column(No_TCN_CabeceraGenericaVentas; "No.")
            {
            }
            column(SelltoCustomerNo_TCN_CabeceraGenericaVentas; "Sell-to Customer No.")
            {
            }
            column(PostingDate_TCN_CabeceraGenericaVentas; "Posting Date")
            {
            }
            column(xCaptionInforme; xCaptionInforme)
            {
            }
            dataitem(ATRTCN_LinGenericaVentasC01; ATRTCN_LinGenericaVentasC01)
            {
                DataItemLink = "Document No." = field("No.");
                column(No_ATRTCN_LinGenericaVentasC01; "No.")
                {
                }
                column(Quantity_ATRTCN_LinGenericaVentasC01; Quantity)
                {
                }
                column(Description_ATRTCN_LinGenericaVentasC01; Description)
                {
                }
                trigger OnAfterGetRecord()
                var
                    myInt: Integer;
                begin

                end;
            }
            trigger OnAfterGetRecord()
            var
                myInt: Integer;
            begin

            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    procedure SetiTipoInformeF(pOption: Option factura,albaran)
    begin
        xOpcion := pOption;
    end;

    var
        xOpcion: Option factura,albaran;
        xCaptionInforme: Text;
}