pageextension 50114 "ATRTCNCExtenesioC01" extends "Vendor List"
{
    layout
    {


    }
    actions
    {
        addlast(processing)
        {
            action(ATRImprimirFacturaC01)
            {
                trigger OnAction()
                var
                    myInt: Integer;
                begin
                    rSalesInvoiceHeader.SetRange("No.", '103040');
                    repGenericoVentas.SetTableView(rSalesInvoiceHeader);
                    repGenericoVentas.Run();
                end;
            }
            action(ATRImprimirAlbaranC01)
            {
                trigger OnAction()
                var
                    myInt: Integer;
                begin
                    rSalesShipmentHeader.SetRange("No.", '102056');
                    repGenericoVentas.SetTableView(rSalesShipmentHeader);
                    repGenericoVentas.Run();
                end;
            }
        }
    }
    var
        rSalesShipmentHeader: Record "Sales Shipment Header";
        rSalesInvoiceHeader: Record "Sales Invoice Header";
        repGenericoVentas: Report ATRTCNGenericoVentasC01;
}
