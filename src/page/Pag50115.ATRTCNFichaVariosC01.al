page 50115 "ATRTCNFichaVariosC01"
{
    Caption = 'Ficha Varios';
    PageType = Card;
    SourceTable = ATRTCNVariosC01;
    AdditionalSearchTerms = 'Curso 01';
    AboutTitle = 'Ficha de tabla varios';
    AboutText = 'Se usará para editar registros de una tabla donde tendremos tipos varios de registros';

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código de vacuna field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripcion de la vacuna field.';
                    ApplicationArea = All;
                }
                field(TipoVacuna; Rec.TipoVacuna)
                {
                    ToolTip = 'Specifies the value of the Tipo de vacuna field.';
                    ApplicationArea = All;
                    AboutTitle = 'Tipo de registro';
                    AboutText = 'Indicara si es registro de vacunos o otros.';
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
                field(PeriodoSigVacunacion; Rec.PeriodoSigVacunacion)
                {
                    ToolTip = 'Specifies the value of the Periodo segunda vacunacion field.';
                    ApplicationArea = All;
                }
            }
            group(informacion)
            {
                Editable = false;
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
            }
        }
    }
}
