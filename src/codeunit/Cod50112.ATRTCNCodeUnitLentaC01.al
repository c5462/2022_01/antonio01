codeunit 50112 "ATRTCNCodeUnitLentaC01"
{
    trigger OnRun()
    begin
        EjecutarTareaF();
    end;


    local procedure EjecutarTareaF()
    var
        xlEjecutar: Boolean;
    begin
        if GuiAllowed then begin //sino no sale el confirm.
            xlEjecutar := Confirm('¿?');
        end;
        if xlEjecutar then begin
            InsertarLogF('tarea iniciada');
            Commit(); //no usar mas
            Message('En proceso');
            Sleep(10000);
            InsertarLogF('tarea finalizada');
        end;
    end;

    local procedure InsertarLogF(pMensaje: Text)
    var
        rlATRTCNLogC01: Record ATRTCNLogC01;
    begin
        rlATRTCNLogC01.Init();
        rlATRTCNLogC01.Insert(true);
        rlATRTCNLogC01.Validate(Mensaje, pMensaje);
        rlATRTCNLogC01.Modify(true);
    end;
}