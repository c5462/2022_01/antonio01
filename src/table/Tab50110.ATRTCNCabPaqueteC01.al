table 50110 "ATRTCNCabPaqueteC01"
{
    Caption = 'CabPaquete';
    DataClassification = SystemMetadata;

    fields
    {
        field(10; Id; Code[20])
        {
            Caption = 'Id';
            DataClassification = SystemMetadata;
        }
        field(20; CodAlmacen; Code[10])
        {
            Caption = 'CodAlmacén';
            DataClassification = SystemMetadata;
            TableRelation = Location.Code;
        }
        field(30; CodUbicacion; Code[20])
        {
            Caption = 'CodUbicacion';
            DataClassification = SystemMetadata;
            TableRelation = Bin.Code where("Location Code" = field(CodAlmacen));
        }
        field(40; CodProducto; Code[20])
        {
            Caption = 'Cod. producto';
            FieldClass = FlowField;
            CalcFormula = lookup(ATRTCNLinPaqueteC01.CodProducto where(IdPaquete = field(Id)));
            Editable = false;
        }
    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
}
