page 50110 "ATRTCNListaPaquetesC01"
{
    ApplicationArea = All;
    Caption = 'TCNListaPaquetes';
    PageType = List;
    SourceTable = ATRTCNCabPaqueteC01;
    UsageCategory = Lists;
    CardPageId = ATRTCNFichaPaqueteC01;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Id; Rec.Id)
                {
                    ToolTip = 'Specifies the value of the Id field.';
                    ApplicationArea = All;
                }
                field(CodAlmacen; Rec.CodAlmacen)
                {
                    ToolTip = 'Specifies the value of the CodAlmacén field.';
                    ApplicationArea = All;
                }
                field(CodUbicacion; Rec.CodUbicacion)
                {
                    ToolTip = 'Specifies the value of the CodUbicacion field.';
                    ApplicationArea = All;
                }
                field(CodProducto; Rec.CodProducto)
                {
                    ToolTip = 'Specifies the value of the Cod. producto field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
