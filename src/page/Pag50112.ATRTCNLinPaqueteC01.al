page 50112 "ATRTCNLinPaqueteC01"
{
    Caption = 'TCNLinPaquete';
    PageType = ListPart;
    SourceTable = ATRTCNLinPaqueteC01;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(IdPaquete; Rec.IdPaquete)
                {
                    ToolTip = 'Specifies the value of the Id paquete field.';
                    ApplicationArea = All;
                }
                field(NumLinea; Rec.NumLinea)
                {
                    ToolTip = 'Specifies the value of the Nº Línea field.';
                    ApplicationArea = All;
                }
                field(CodProducto; Rec.CodProducto)
                {
                    ToolTip = 'Specifies the value of the Cód. Producto field.';
                    ApplicationArea = All;
                }
                field(CodVariante; Rec.CodVariante)
                {
                    ToolTip = 'Specifies the value of the Cód. Variante field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
