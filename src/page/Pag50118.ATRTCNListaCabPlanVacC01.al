page 50118 "ATRTCNListaCabPlanVacC01"
{
    ApplicationArea = All;
    Caption = 'Lista Planificacion Vacunacion';
    PageType = List;
    SourceTable = ATRTCNCabeceraPlanVacunaC01;
    UsageCategory = Lists;
    AboutTitle = 'Lista Planificacion de vacunacion';
    CardPageId = ATRTCNFichaCabPlanVacunaC01;
    Editable = false;
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Codigo Vacuna field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripcion Vacuna field.';
                    ApplicationArea = All;
                }
                field("FechaIncioVacu.Planif."; Rec."FechaIncioVacu.Planif.")
                {
                    ToolTip = 'Specifies the value of the Fecha Incio Vacunación Planificada field.';
                    ApplicationArea = All;
                }
                field(Empresavacunadora; Rec.Empresavacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre de Empresa Vacunadora';
                    ToolTip = 'Aqui aparecerá el nombre del proveedor que realizará la vacunación.';
                    ApplicationArea = All;
                }

            }
        }
    }

    procedure GetSelectionFilterF(var xSalida: Record ATRTCNCabeceraPlanVacunaC01)
    begin
        CurrPage.SetSelectionFilter(xSalida);
    end;
}
