pageextension 50117 "ATRTCNListadoEmpresasExtC01" extends Companies
{
    layout
    {
        addafter("Display Name")
        {
            field(ATRClienteC01; CuentaF(rlCustomer))
            {
                ApplicationArea = All;
                Caption = 'Numero de Clientes';
                trigger OnDrillDown()
                begin
                    rlCustomer.ChangeCompany(Rec.Name);
                    Page.Run(0, rlCustomer);
                end;
            }
            field(ATRProveedorC01; cuentaF(rlVendor))
            {
                ApplicationArea = all;
                Caption = 'Numero de Proveedores';
                trigger OnDrillDown()
                begin
                    rlVendor.ChangeCompany(Rec.Name);
                    Page.Run(0, rlVendor);
                end;
            }
            field(ATRCuentaC01; cuentaF(rlCuenta))
            {
                ApplicationArea = all;
                Caption = 'Numero de cuentas';
                trigger OnDrillDown();
                begin
                    rlCuenta.ChangeCompany(Rec.Name);
                    page.Run(0, rlCuenta);
                end;
            }
            field(ATRProductosC01; cuentaF(rlProducto))
            {
                ApplicationArea = all;
                Caption = 'Numero de productos';
                trigger OnDrillDown();
                begin
                    rlProducto.ChangeCompany(Rec.Name);
                    page.Run(0, rlProducto);
                end;
            }
        }
    }
    actions
    {
        addlast(processing)
        {

            action(ATRCopiaSeguridadC01)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin
                    CopiaSeguridadF();
                end;
            }
            action(ATRRestaurarCopiaSeguridadC01)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin
                    RestaurarCopiaSeguridadF();
                end;
            }
        }
    }
    var
        rlCustomer: Record Customer;
        rlVendor: Record Vendor;
        rlCuenta: Record "G/L Account";
        rlProducto: Record Item;
        rlCompany: Record Company;

    local procedure CuentaF(pVendor: Record Vendor): Integer
    begin
        if rlCompany.Get(Rec.Name) then begin
            if Rec.Name <> CompanyName then begin
                pVendor.ChangeCompany(Rec.Name);
            end;
        end;
        exit(pVendor.Count);
    end;

    local procedure CuentaF(pCustomer: Record Customer): Integer
    begin
        if rlCompany.Get(Rec.Name) then begin
            if Rec.Name <> CompanyName then begin
                pCustomer.ChangeCompany(Rec.Name);
            end;
        end;
        exit(pCustomer.Count);
    end;

    local procedure CuentaF(pCuenta: Record "G/L Account"): Integer
    begin
        if rlCompany.Get(Rec.Name) then begin
            if Rec.Name <> CompanyName then begin
                pCuenta.ChangeCompany(Rec.Name);
            end;
        end;
        exit(pCuenta.Count);
    end;

    local procedure CuentaF(pItem: Record Item): Integer
    begin
        if rlCompany.Get(Rec.Name) then begin
            if Rec.Name <> CompanyName then begin
                pItem.ChangeCompany(Rec.Name);
            end;
        end;
        exit(pItem.Count);
    end;

    local procedure EjemploRecordRefsF() //Los recordRefs Solo reconocen registros, no reconoce campos en si.
    var
        xlRecRef: RecordRef;
        i: Integer;
    begin
        xlRecRef.Open(Database::Item);
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 0 to xlRecRef.FieldCount do begin
                    message('%1', xlRecRef.FieldIndex(i).Value); //fieldindex actua como iterador y field devuelve el numero del campo en si, con value nos da el valor del campo.
                end;
                xlRecRef.Field(3).Validate(format(xlRecRef.Field(3).Value).ToUpper());
                xlRecRef.Modify(true);
            until xlRecRef.Next() = 0;
        end;
    end;

    local procedure CopiaSeguridadF()
    var
        rlCompany: Record Company;
        rlAllObjWithCaption: Record AllObjWithCaption;
        xlTextBuilder: TextBuilder;
        xlRecRef: RecordRef;
        culTypeHelper: Codeunit "Type Helper";
        i: Integer;
        xlNombreFichero: Text;
        TempLATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        xlInStr: Instream;
        xlOutStr: OutStream;
    begin
        CurrPage.SetSelectionFilter(rlCompany);
        if rlCompany.FindSet(false) then begin
            repeat
                xlTextBuilder.Append('IE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
                //Escribir en el fichero
                rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
                rlAllObjWithCaption.SetFilter("Object ID", '%1|%2|%3|%4', Database::Customer, Database::"G/L Account", Database::Vendor, Database::Item);
                if rlAllObjWithCaption.FindSet(false) then begin
                    repeat
                        xlTextBuilder.Append('IT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                        xlRecRef.Open(rlAllObjWithCaption."Object ID", false, rlCompany.Name);
                        if xlRecRef.FindSet(false) then begin
                            repeat
                                xlTextBuilder.Append('IR <' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                                for i := 1 to xlRecRef.FieldCount do begin
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin
                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField();
                                        end;
                                        if Format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuilder.Append('<' + xlRecRef.FieldIndex(i).Name + '> · <' + Format(xlRecRef.FieldIndex(i).Value) + '>' + culTypeHelper.CRLFSeparator());
                                        end;
                                    end;
                                    // xlRecRef.FieldIndex(i).Value;
                                end;
                                xlTextBuilder.Append('FR<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                            until xlRecRef.Next() = 0;
                        end;
                        xlRecRef.Close();
                        xlTextBuilder.Append('FT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                    until rlAllObjWithCaption.Next() = 0;
                end;

                //Bucle de tablas
                xlTextBuilder.Append('FE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
            until rlCompany.Next() = 0;
            TempLATRTCNConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
            xlOutStr.WriteText(xlTextBuilder.ToText());
            xlNombreFichero := 'CopiaDatos.csv';
            TempLATRTCNConfiguracionC01.MiBlob.CreateInStream(xlInStr);
            if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
                Error('Error 404');
            end;
        end;
    end;

    local procedure ValorCampoF(pFieldRef: FieldRef; pValor: Text): Text
    begin
        if pFieldRef.Type = pFieldRef.Type::Option then begin

        end else begin
            exit(pValor);
        end;
    end;

    local procedure RestaurarCopiaSeguridadF()
    var
        TempLATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        xlInStr: Instream;
        xlLinea: Text;
        rlAllObjWithCaption: Record AllObjWithCaption;
        xlRecRef: RecordRef;
        clSeparador: Label '·';
    begin
        //1) Subir fichero de copias de seguridad
        TempLATRTCNConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            //2) Recorrer el fichero
            while not xlInStr.EOS do begin
                xlInStr.ReadText(xlLinea);
                case true of
                    //3) Si es inicio de tabla, abrir el registro
                    copystr(xlLinea, 1, 3) = 'IT<':
                        xlRecRef.Open(NumTablaF(CopyStr(xlLinea, 4).TrimEnd('>')));
                    //4) Si es inicio de registro inicializamos el registro
                    copystr(xlLinea, 1, 3) = 'IR<':
                        xlRecRef.Init();
                    //5) Por cada campo asigna su valor
                    xlLinea.StartsWith('<'):
                        if xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split(clSeparador).Get(1).TrimStart(' ').TrimEnd(' ').TrimStart('<').TrimEnd('>'))).Class = FieldClass::Normal then begin
                            xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split(clSeparador).Get(1).TrimStart(' ').TrimEnd(' ').TrimStart('<').TrimEnd('>'))).Value := CampoYValorF(xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split(clSeparador).Get(1).TrimStart(' ').TrimEnd(' ').TrimStart('<').TrimEnd('>'))), xlLinea.Split(clSeparador).Get(2).TrimStart(' ').TrimEnd(' ').TrimStart('<').TrimEnd('>'));
                        end;
                    //6) Si es el fin de registros, insertar el registro
                    copystr(xlLinea, 1, 3) = 'FR<':
                        xlRecRef.Insert(false); //true cuando queremos meter ficheros reales pero en este caso metemos false para no comprobar claves primarias y asi ir mas rapido
                    //7) Si es fin de tabla cerrar el registro
                    copystr(xlLinea, 1, 3) = 'FT<':
                        xlRecRef.Close();
                end;
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor');
        end;
    end;

    local procedure NumTablaF(pNombreTabla: Text): Integer
    var
        rlAllObjWithCaption: Record AllObjWithCaption;
    begin
        rlAllObjWithCaption.SetRange("Object Name", pNombreTabla);
        rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
        rlAllObjWithCaption.SetLoadFields("Object ID");
        if rlAllObjWithCaption.FindFirst() then begin
            exit(rlAllObjWithCaption."Object ID");
        end;
    end;

    local procedure NumCampoF(pNumTabla: Integer; pNombreCampo: Text): Integer
    var
        rlField: Record Field;
    begin
        rlField.SetRange(TableNo, pNumTabla);
        rlField.SetRange(FieldName, pNombreCampo);
        rlField.SetLoadFields("No.");
        if rlField.FindFirst() then begin
            exit(rlField."No.");
        end;
    end;

    local procedure ATRCampoYValorF(pFieldRef: FieldRef; pValor: Text): Variant
    var

        xlBigInteger: BigInteger;
        rlField: Record Field;
        i: Integer;
        xlBool: Boolean;
        xlDec: Decimal;
        xlDate: DateTime;
        TempLATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        xlTime: Time;
        xlDateTime: Time;
        TempLCustomer: Record Customer temporary;
    begin
        case pFieldRef.Type of
            pFieldRef.Type::Code, pFieldRef.Type::Text:
                exit(pValor);
            pFieldRef.Type::BigInteger, pFieldRef.Type::Guid:
                if Evaluate(xlBigInteger, pValor) then begin
                    exit(xlBigInteger);
                end;
            pFieldRef.Type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin
                        if pFieldRef.GetEnumValueCaption(i).ToLower() = pValor.ToLower() then begin
                            exit(i);
                        end;
                    end;
                end;
            pFieldRef.Type::Boolean:
                if Evaluate(xlBool, pValor) then begin
                    exit(xlBool);
                end;
            pFieldRef.Type::Decimal:
                if Evaluate(xlDec, pValor) then begin
                    exit(xlDec);
                end;
            pFieldRef.Type::Blob:
                exit(TempLATRTCNConfiguracionC01.MiBlob);
            pFieldRef.Type::DateTime:
                if Evaluate(xlDateTime, pValor) then begin
                    exit(xlDateTime);
                end;
            pFieldRef.Type::Time:
                if Evaluate(xlTime, pValor) then begin
                    exit(xlTime);
                end;
            pFieldRef.Type::Date:
                if Evaluate(xlDate, pValor) then begin
                    exit(xlDate)
                end;
            pFieldRef.Type::Media:
                exit(TempLCustomer.Image);
            pFieldRef.Type::MediaSet:
                exit(TempLCustomer.Image);
            else begin
                    exit(pValor);
                end;
        end;
    end;

    local procedure CampoYValorF(pFieldRef: FieldRef; pValor: Text): Variant
    var
        TempLCustomer: Record Customer temporary;
        TempLJJConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        TempLItem: Record Item temporary;
        xlBigInteger: BigInteger;
        xlBool: Boolean;
        xlDecimal: Decimal;
        xlGuid: Guid;
        i: Integer;
        xlDateTime: DateTime;
        xlDate: Date;
        xlTime: Time;
    begin
        case pFieldRef.Type of
            pFieldRef.Type::BigInteger, pFieldRef.Type::Integer:
                if Evaluate(xlBigInteger, pValor) then begin
                    exit(xlBigInteger);
                end;
            pFieldRef.Type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin if pFieldRef.GetEnumValueCaption(i).ToLower() = pValor.ToLower() then begin exit(i); end; end;
                end;
            pFieldRef.Type::Boolean:
                if Evaluate(xlBool, pValor) then begin
                    exit(xlBool);
                end;
            pFieldRef.Type::Blob:
                exit(TempLJJConfiguracionC01.MiBlob);
            pFieldRef.Type::Decimal:
                if Evaluate(xlDecimal, pValor) then begin
                    exit(xlDecimal);
                end;
            pFieldRef.Type::Guid:
                if Evaluate(xlGuid, pValor) then begin
                    exit(xlGuid);
                end;
            pFieldRef.Type::DateTime:
                if Evaluate(xlDateTime, pValor) then begin
                    exit(xlDateTime);
                end;
            pFieldRef.Type::Date:
                if Evaluate(xlDate, pValor) then begin
                    exit(xlDate);
                end;
            pFieldRef.Type::Media:
                exit(TempLCustomer.Image);
            pFieldRef.Type::MediaSet:
                exit(TempLItem.Picture);
            pFieldRef.Type::Time:
                if Evaluate(xlTime, pValor) then begin
                    exit(xlTime);
                end;
            else
                exit(pValor);
        end;
    end;
}
