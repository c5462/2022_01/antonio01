pageextension 50111 "ATRTCNCustomerCardExtC01" extends "Customer Card"
{
    layout
    {
        addlast(content)
        {
            group(ATRTCNVacunasC01)
            {
                Caption = 'Vacunas';

                field(ATRTCNNumVacunasC01; Rec.ATRTCNNumVacunasC01)
                {
                    ToolTip = 'Numero de vacunas que tiene suministradas';
                    ApplicationArea = All;

                }
                field(ATRTCNTipoVacunaC01; Rec.ATRTCNTipoVacunaC01)
                {
                    ToolTip = 'Tipo de vacunas que tiene suministradas';
                    ApplicationArea = All;
                }
                field(ATRTCNFechaUltimaVacunaC01; Rec.ATRTCNFechaUltimaVacunaC01)
                {
                    ToolTip = 'Fecha de la vacuna que ha sido suministrada la ultima';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            action(ATRTCNAsignarVacunaBloqueadaC01)
            {
                ApplicationArea = All;
                Caption = '💉Asignar vacuna bloqueada💉';
                trigger OnAction()
                begin
                    Rec.Validate(ATRTCNTipoVacunaC01, 'BLOQUEADA');
                end;
            }
            action(ATRTCNSwitchVariableC01)
            {
                Caption = 'Prueba Variables globales a BC';
                ApplicationArea = all;
                trigger OnAction()
                var
                    clMensaje: Label 'El valor actual es %1. Lo cambiamos a %2';
                begin
                    Message(clMensaje, cuATRFuncionesAppC01.SwitchF(), not cuATRFuncionesAppC01.SwitchF());
                    cuATRFuncionesAppC01.SwitchF(not cuATRFuncionesAppC01.SwitchF());
                end;
            }
            action(ATRConvertirClienteAProveedorC01)
            {
                Caption = 'Convertir este cliente a proveedor';
                ApplicationArea = all;
                trigger OnAction()
                var
                    rlVendor: Record Vendor;
                begin
                    rlVendor.Init();
                    rlVendor.TransferFields(Rec, true, true);
                    rlVendor.Insert(true);
                    Message('Cliente %1 Convertido a proveedor', Rec.Name);
                    Page.Run(0, rlVendor);
                end;
            }
        }
    }
    var
        cuATRFuncionesAppC01: Codeunit "ATRTCNVariablesGlobalesC01";

}
