report 50113 "ATRTCNDocumentoVentaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'documentoventa.rdl';

    dataset
    {
        dataitem("Sales Header"; "Sales Header")
        {
            RequestFilterFields = "No.", "Document Type";
            column(No_SalesHeader; "No.")
            {
            }
            column(SelltoCustomerNo_SalesHeader; "Sell-to Customer No.")
            {
            }
            column(PostingDate_SalesHeader; "Posting Date")
            {
            }
            column(xDocumentLabel; xDocumentLabel)
            {
            }
            column(xCustAddr1; xCustAddr[1])
            {
            }
            column(xCustAddr2; xCustAddr[2])
            {
            }
            column(xCustAddr3; xCustAddr[3])
            {
            }
            column(xCustAddr4; xCustAddr[4])
            {
            }
            column(xCustAddr5; xCustAddr[5])
            {
            }
            column(xCustAddr6; xCustAddr[6])
            {
            }
            column(xCustAddr7; xCustAddr[7])
            {
            }
            column(xCustAddr8; xCustAddr[8])
            {
            }
            column(xCompanyAddr1; xCompanyAddr[1])
            {
            }
            column(xCompanyAddr2; xCompanyAddr[2])
            {
            }
            column(xCompanyAddr3; xCompanyAddr[3])
            {
            }
            column(xCompanyAddr4; xCompanyAddr[4])
            {
            }
            column(xCompanyAddr5; xCompanyAddr[5])
            {
            }
            column(xCompanyAddr6; xCompanyAddr[6])
            {
            }
            column(xCompanyAddr7; xCompanyAddr[7])
            {
            }
            column(xCompanyAddr8; xCompanyAddr[8])
            {
            }
            column(Logo; rCompanyInfo.Picture)
            {
            }
            column(xTotalIva11; xTotalIva[1, 1])
            {
            }
            column(xTotalIva21; xTotalIva[2, 1])
            {
            }
            column(xTotalIva31; xTotalIva[3, 1])
            {
            }
            column(xTotalIva41; xTotalIva[4, 1])
            {
            }
            column(xTotalIva12; xTotalIva[1, 2])
            {
            }
            column(xTotalIva22; xTotalIva[2, 2])
            {
            }
            column(xTotalIva32; xTotalIva[3, 2])
            {
            }
            column(xTotalIva42; xTotalIva[4, 2])
            {
            }
            column(xTotalIva13; xTotalIva[1, 3])
            {
            }
            column(xTotalIva23; xTotalIva[2, 3])
            {
            }
            column(xTotalIva33; xTotalIva[3, 3])
            {
            }
            column(xTotalIva43; xTotalIva[4, 3])
            {
            }
            column(xTotalIva14; xTotalIva[1, 4])
            {
            }
            column(xTotalIva24; xTotalIva[2, 4])
            {
            }
            column(xTotalIva34; xTotalIva[3, 4])
            {
            }
            column(xTotalIva445; xTotalIva[4, 4])
            {
            }
            dataitem(Copias; Integer)
            {
                column(Number_Copias; Number)
                {
                }
                dataitem("Sales Line"; "Sales Line")
                {
                    // DataItemLink = "Document No." = field("No."), "Document Type" = field("Document Type");
                    column(No_SalesLine; "No.")
                    {
                    }
                    column(Description_SalesLine; Description)
                    {
                    }
                    column(Quantity_SalesLine; Quantity)
                    {
                    }
                    column(UnitPrice_SalesLine; "Unit Price")
                    {
                    }
                    column(LineDiscount_SalesLine; "Line Discount %")
                    {
                    }
                    column(Amount_SalesLine; Amount)
                    {
                    }
                    column(LineNo_SalesLine; "Line No.")
                    {
                    }
                    dataitem("Extended Text Line"; "Extended Text Line")
                    {
                        DataItemLink = "No." = field("No.");
                        column(Text_ExtendedTextLine; "Text")
                        {
                        }
                    }
                    dataitem("Item Cross Reference"; "Item Cross Reference")
                    {
                        DataItemLink = "item No." = field("No.");
                        DataItemTableView = where("Cross-reference type" = filter(Customer));
                        column(CrossReferenceNo_ItemCrossReference; "Cross-Reference No.")
                        {
                        }
                        column(Description_ItemCrossReference; Description)
                        {
                        }
                    }
                    trigger OnPreDataItem() //seria lo mismo que el data item link que hay comentado arriba ya que no nos encuentra los header se hace con este trigger.
                    begin
                        "Sales Line".SetRange("Document No.", rSalesHeader."No.");
                        "Sales Line".SetRange("Document Type", rSalesHeader."Document Type");
                    end;
                }
                trigger OnPreDataItem()
                begin
                    if xCopias = 0 then begin
                        xCopias := 1;
                    end;
                    Copias.SetRange(Number, 0, xCopias);
                end;
            }
            trigger OnAfterGetRecord()
            begin
                case "Sales Header"."Document Type" of
                    "Sales Header"."Document Type"::Quote:
                        begin
                            cuFormatAddr.SalesHeaderSellTo(xCustAddr, "Sales Header");
                            xDocumentLabel := 'Nº Oferta';
                        end;
                    "Sales Header"."Document Type"::Order:
                        begin
                            cuFormatAddr.SalesHeaderSellTo(xCustAddr, "Sales Header");
                            xDocumentLabel := 'Nº Pedido';
                        end;
                    "Sales Header"."Document Type"::Invoice:
                        begin
                            cuFormatAddr.SalesHeaderSellTo(xCustAddr, "Sales Header");
                            xDocumentLabel := 'Nº Factura';
                        end;
                end;
                CalcIVA();
            end;

            trigger OnPreDataItem()
            begin
                "Sales Header".SetRange("Document Type");
            end;
        }
    }
    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(rSalesHeader;
                    rSalesHeader."Document Type")
                    {
                        ApplicationArea = all;
                    }
                    field(xDocumentNo;
                    xDocumentNo)
                    {
                        ApplicationArea = all;
                        trigger OnLookup(var xDocumentoNo: Text):
                    Boolean
                        var
                            plSalesHeader: Page "Sales List";
                        begin
                            rSalesHeader.SetRange("Document Type", rSalesHeader."Document Type");
                            plSalesHeader.SetTableView(rSalesHeader); //aplicar filtros a la tabla de la pagina
                            plSalesHeader.LookupMode := true; //pongo la pagina en modo lookup
                            if plSalesHeader.RunModal() = Action::LookupOK then begin //Ejecuto la pagina y controlo la accion del usuario
                                plSalesHeader.SetSelectionFilter(rSalesHeader); //aplico la seleccion del usuario de la pagina a la tabla
                                if rSalesHeader.FindSet() then begin //busqueda
                                    repeat
                                        //elbaroacion de filtro
                                        xDocumentNo += rSalesHeader."No." + '|'; //Puesto que concatenamos, no podemos dejar el caracter | al final
                                    until rSalesHeader.Next() = 0;
                                end;
                                xDocumentNo := DelChr(xDocumentNo, '>', '|');  //Entonces limpiamos aqui con DelChr, Simepre se limpia
                            end;
                        end;
                    }
                    field(Copias2; xCopias)
                    {
                        ApplicationArea = all;
                    }
                }
            }
        }

    }
    trigger OnPreReport()
    begin
        rCompanyInfo.Get();
        rCompanyInfo.CalcFields(Picture);
        cuFormatAddr.Company(xCompanyAddr, rCompanyInfo);
    end;

    procedure CalcIVA()
    var
        rlSalesLine: Record "Sales Line";
        rlTempVATAmount: Record "VAT Amount Line" temporary;
        i: Integer;
    begin
        rlSalesLine.SetRange("Document No.", "Sales Header"."No.");
        rlSalesLine.SetRange("Document Type", "Sales Header"."Document Type");

        if rlSalesLine.FindSet() then begin
            repeat
                rlTempVATAmount.SetRange("VAT Identifier", rlSalesLine."VAT Identifier");

                if rlTempVATAmount.FindFirst() then begin
                    rlTempVATAmount."VAT Base" += rlSalesLine.Amount;
                    rlTempVATAmount."VAT %" := rlSalesLine."VAT %";
                    rlTempVATAmount."VAT Amount" += rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATAmount."Amount Including VAT" += rlSalesLine."Amount Including VAT";
                    rlTempVATAmount."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVATAmount.Modify(false);
                end else begin
                    rlTempVATAmount."VAT Base" := rlSalesLine.Amount;
                    rlTempVATAmount."VAT %" := rlSalesLine."VAT %";
                    rlTempVATAmount."VAT Amount" := rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATAmount."Amount Including VAT" := rlSalesLine."Amount Including VAT";
                    rlTempVATAmount."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVATAmount.Insert(false);
                end;
            until rlSalesLine.Next() = 0;
        end;
        Clear(xTotalIva);
        rlTempVATAmount.FindSet();
        for i := 1 to 4 do begin
            xTotalIva[1, 1] := Format(rlTempVATAmount."VAT Base");
            xTotalIva[2, 1] := Format(rlTempVATAmount."VAT %");
            xTotalIva[3, 1] := Format(rlTempVATAmount."VAT Amount");
            xTotalIva[4, 1] := Format(rlTempVATAmount."Amount Including VAT");
            if rlTempVATAmount.Next() = 0 then begin
                i := 1000;
            end;
        end;
    end;


    var
        rSalesHeader: Record "Sales Header";
        xDocumentNo: Text;
        xCustAddr: array[8] of Text;
        xCompanyAddr: array[8] of Text;
        cuFormatAddr: Codeunit "Format Address";
        rCompanyInfo: Record "Company Information";
        xDocumentLabel: Text;
        xCopias: Integer;
        xTotalIva: array[4, 4] of Text;
}