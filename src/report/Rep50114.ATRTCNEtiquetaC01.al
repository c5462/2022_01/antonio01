report 50114 "ATRTCNEtiquetaC01"
{
    ApplicationArea = All;
    Caption = 'TCNEtiquetaC01';
    UsageCategory = Administration;
    DefaultLayout = RDLC;
    RDLCLayout = 'etiqueta.rdl';
    //UseRequestPage = false;
    dataset
    {
        dataitem("Sales Shipment Line"; "Sales Shipment Line")
        {
            column(DocumentNo_SalesShipmentLine; "Document No.")
            {
            }
            column(No_SalesShipmentLine; "No.")
            {
            }
            column(Description_SalesShipmentLine; Description)
            {
            }
            dataitem(Integer; Integer)
            {
                column(Number_Integer; Number)
                {
                }
                trigger OnPreDataItem()
                begin
                    Integer.SetRange(Number, 1, "Sales Shipment Line".Quantity);
                end;
            }
        }
    }
    requestpage
    {
        layout
        {
            area(content)
            {
                group(GroupName)
                {
                }
            }
        }
        actions
        {
            area(processing)
            {
            }
        }
    }
}
