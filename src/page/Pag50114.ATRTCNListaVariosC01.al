page 50114 "ATRTCNListaVariosC01"
{
    Caption = 'Lista de Varios';
    AdditionalSearchTerms = 'Curso 01';
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;
    Editable = false;
    PageType = List;
    SourceTable = ATRTCNVariosC01;
    UsageCategory = Lists;
    CardPageId = ATRTCNFichaVariosC01;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código de vacuna field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripcion de la vacuna field.';
                    ApplicationArea = All;
                }
                field(TipoVacuna; Rec.TipoVacuna)
                {
                    ToolTip = 'Specifies the value of the Tipo de vacuna field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
                field(PeriodoSigVacunacion; Rec.PeriodoSigVacunacion)
                {
                    ToolTip = 'Specifies the value of the Periodo segunda vacunacion field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
