report 50111 "ATRTCNListadoProyectosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'Listadoproyectos.rdl';
    AdditionalSearchTerms = 'Listado Proyectos';
    Caption = 'Listado Proyectos';
    dataset
    {
        dataitem(Job; Job)
        {
            RequestFilterFields = "No.", "Creation Date", Status;
            column(JobNo_; "No.")
            {
            }
            column(BilltoCustomerNo_DataItemName; "Bill-to Customer No.")
            {
            }
            column(Description_DataItemName; Description)
            {
            }
            column(CreationDate_DataItemName; "Creation Date")
            {
            }
            column(Status_DataItemName; Status)
            {
            }
            column(Color; xColor)
            {
            }
            dataitem(JobTask; "Job Task")
            {
                RequestFilterFields = "Job Task No.", "Job Task Type";
                DataItemLink = "Job No." = field("No.");
                column(JobTaskNo_JobTask; "Job Task No.")
                {
                }
                column(JobTaskType_JobTask; "Job Task Type")
                {
                }
                column(Description_JobTask; Description)
                {
                }
                column(StartDate_JobTask; "Start Date")
                {
                }
                column(EndDate_JobTask; "End Date")
                {
                }
                dataitem(JobPlanningLine; "Job Planning Line")
                {
                    RequestFilterFields = "No.", "Planning Date", Type;
                    DataItemLink = "Job No." = field("Job No."), "Job Task No." = field("Job Task No.");
                    column(No_JobPlanningLine; "No.")
                    {
                    }
                    column(Line_No_; "Line No.")
                    {
                    }
                    column(Description_JobPlanningLine; Description)
                    {
                    }
                    column(Quantity_JobPlanningLine; Quantity)
                    {
                    }
                    column(PlanningDate_JobPlanningLine; "Planning Date")
                    {
                    }
                    column(LineAmount_JobPlanningLine; "Line Amount")
                    {
                    }
                    column(Type_JobPlanningLine; "Type")
                    {
                    }
                    dataitem("Job Ledger Entry"; "Job Ledger Entry")
                    {
                        DataItemLink = "Entry No." = field("Job Ledger Entry No.");
                        column(DocumentNo_JobLedgerEntry; "Document No.")
                        {
                        }
                        column(DocumentDate_JobLedgerEntry; Format("Document Date"))
                        {
                        }
                        column(Type_JobLedgerEntry; "Type")
                        {
                        }
                        column(EntryType_JobLedgerEntry; "Entry Type")
                        {
                        }
                        column(LedgerEntryType_JobLedgerEntry; "Ledger Entry Type")
                        {
                        }
                        column(LineType_JobLedgerEntry; "Line Type")
                        {
                        }
                    }
                }
            }
            trigger OnAfterGetRecord()
            begin
                case Job.Status of
                    Job.Status::Completed:
                        begin
                            xColor := 'Green';
                        end;
                    Job.Status::Open:
                        begin
                            xColor := 'Blue';
                        end;
                    Job.Status::Planning:
                        begin
                            xColor := 'Yellow';
                        end;
                    Job.Status::Quote:
                        begin
                            xColor := 'Red';
                        end;
                end;
            end;

        }

    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        xColor: Text[20];
}