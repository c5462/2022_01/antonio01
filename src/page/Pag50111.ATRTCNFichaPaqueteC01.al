page 50111 "ATRTCNFichaPaqueteC01"
{
    Caption = 'TCNFichaPaquete';
    PageType = Card;
    SourceTable = ATRTCNCabPaqueteC01;
    UsageCategory = Documents;
    ApplicationArea = All;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Id; Rec.Id)
                {
                    ToolTip = 'Specifies the value of the Id field.';
                    ApplicationArea = All;
                }
                field(CodAlmacen; Rec.CodAlmacen)
                {
                    ToolTip = 'Specifies the value of the CodAlmacén field.';
                    ApplicationArea = All;
                }
                field(CodUbicacion; Rec.CodUbicacion)
                {
                    ToolTip = 'Specifies the value of the CodUbicacion field.';
                    ApplicationArea = All;
                }
                field(CodProducto; Rec.CodProducto)
                {
                    ToolTip = 'Specifies the value of the Cod. producto field.';
                    ApplicationArea = All;
                }
            }
            group(TCNLinPaquete)
            {
                part(LineaPaquete; ATRTCNLinPaqueteC01)
                {
                    Caption = 'Lineas de paquete';
                    ApplicationArea = All;
                }
            }
        }
    }
}
