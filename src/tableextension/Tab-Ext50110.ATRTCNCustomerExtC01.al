tableextension 50110 "ATRTCNCustomerExtC01" extends Customer
{
    fields
    {
        field(50110; ATRTCNNumVacunasC01; Integer)
        {
            Caption = 'Numero de vacunas';
            //DataClassification = CustomerContent;
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = count(ATRTCNLinPlanVacunC01 where(ClienteAVacunar = field("No.")));
        }
        field(50111; ATRTCNFechaUltimaVacunaC01; Date)
        {
            Caption = 'Fecha de la última vacuna';
            //DataClassification = CustomerContent;
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = max(ATRTCNLinPlanVacunC01.FechaVacunacion where(ClienteAVacunar = field("No.")));
        }
        field(50112; ATRTCNTipoVacunaC01; Code[20])
        {
            Caption = 'Tipo de vacuna';
            DataClassification = CustomerContent;
            TableRelation = ATRTCNVariosC01.Codigo where(TipoVacuna = const(Vacuna));
            trigger OnValidate()
            var
                rlATRTCNVariosC01: Record ATRTCNVariosC01;
            begin
                if (rlATRTCNVariosC01.Get(rlATRTCNVariosC01.TipoVacuna::Vacuna, Rec.ATRTCNTipoVacunaC01)) then begin
                    rlATRTCNVariosC01.TestField(Bloqueado, false); //manda un mensaje de error diciendo que bloqueado debe ser false
                end;
            end;
        }
        modify(Name)
        {
            trigger OnAfterValidate()
            var
                cuATRTCNFuncionesC01: Codeunit "ATRTCNVariablesGlobalesC01";
            begin
                cuATRTCNFuncionesC01.NombreF(Rec.Name);
            end;
        }
    }

    trigger OnAfterInsert()
    var
        rlCompany: Record Company;
        rlCustomer: Record Customer;
    begin
        if rlCompany.FindSet(false) then begin
            repeat
                rlCustomer.ChangeCompany(rlCompany.Name);
                rlCustomer.Init();
                rlCustomer.Copy(Rec);
                rlCustomer.Insert(true);
            until rlCompany.Next() = 0;
        end;
    end;

    trigger OnAfterModify()
    var
        rlCompany: Record Company;
        rlCustomer: Record Customer;
    begin
        if rlCompany.FindSet(false) then begin
            repeat
                rlCustomer.ChangeCompany(rlCompany.Name);
                rlCustomer.Modify(false);
            until rlCompany.Next() = 0;
        end;
    end;

    trigger OnAfterDelete()
    var
        rlCompany: Record Company;
        rlCustomer: Record Customer;
    begin
        if rlCompany.FindSet(false) then begin
            repeat
                rlCustomer.ChangeCompany(rlCompany.Name);
                rlCustomer.Delete(false);
            until rlCompany.Next() = 0;
        end;
    end;
}
