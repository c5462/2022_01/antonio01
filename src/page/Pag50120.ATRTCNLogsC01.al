page 50120 "ATRTCNLogsC01"
{
    ApplicationArea = All;
    Caption = 'TCNListaLogsC01';
    PageType = List;
    SourceTable = ATRTCNLogC01;
    UsageCategory = Lists;
    Editable = false;
    ModifyAllowed = false;
    DeleteAllowed = true;
    InsertAllowed = false;
    SourceTableView = sorting(SystemCreatedAt) order(descending);
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Id; Rec.Id)
                {
                    ToolTip = 'Specifies the value of the Id field.';
                    ApplicationArea = All;
                }
                field(Mensaje; Rec.Mensaje)
                {
                    ToolTip = 'Specifies the value of the Mensaje field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
