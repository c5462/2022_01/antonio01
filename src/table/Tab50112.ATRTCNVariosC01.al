table 50112 "ATRTCNVariosC01"
{
    Caption = 'Varios';
    LookupPageId = ATRTCNFichaVariosC01;
    DrillDownPageId = ATRTCNListaVariosC01;
    DataClassification = SystemMetadata;

    fields
    {
        field(1; TipoVacuna; Enum ATRTCNTipoVacunaC01)
        {
            Caption = 'Tipo de vacuna';
            DataClassification = SystemMetadata;
        }
        field(2; Codigo; Code[20])
        {
            Caption = 'Código de vacuna';
            DataClassification = SystemMetadata;
        }
        field(3; Descripcion; Text[50])
        {
            Caption = 'Descripcion de la vacuna';
            DataClassification = SystemMetadata;
        }
        field(4; Bloqueado; Boolean)
        {
            Caption = 'Bloqueado';
            DataClassification = SystemMetadata;
        }
        field(5; PeriodoSigVacunacion; DateFormula)
        {
            DataClassification = SystemMetadata;
            Caption = 'Periodo segunda vacunacion';
        }

    }
    keys
    {
        key(PK; TipoVacuna, Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion)
        {
        }
    }
    trigger OnDelete()
    begin
        Rec.TestField(Bloqueado, true);
    end;
}
