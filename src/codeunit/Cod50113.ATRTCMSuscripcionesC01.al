codeunit 50113 "ATRTCMSuscripcionesC01"
{
    SingleInstance = true;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnAfterPostSalesDocF(var SalesHeader: Record "Sales Header")
    var
        culATRTCNVariablesGlobalesC01: Codeunit ATRTCNVariablesGlobalesC01;
    begin
        culATRTCNVariablesGlobalesC01.CodeunitSalesPostOnAfterPostSalesDocF(SalesHeader);
    end;
}