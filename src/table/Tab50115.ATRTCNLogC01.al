table 50115 "ATRTCNLogC01"
{
    Caption = 'ATRTCNLogC01';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; Id; Guid)
        {
            Caption = 'Id';
            DataClassification = SystemMetadata;
        }
        field(2; Mensaje; Text[250])
        {
            Caption = 'Mensaje';
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
        key(FK; SystemCreatedAt)
        {

        }
    }
    trigger OnInsert()
    begin
        if IsNullGuid(Rec.Id) then begin
            Rec.Validate(Id, CreateGuid());
        end;
    end;
}
