tableextension 50111 "ATRTCNVendorExtC01" extends Vendor
{
    trigger OnAfterInsert()
    var
        culATRTCNFuncionesC01: Codeunit "ATRTCNVariablesGlobalesC01";
    begin
        Rec.Validate(Name, culATRTCNFuncionesC01.NombreF());
    end;
}