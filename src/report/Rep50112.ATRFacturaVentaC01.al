report 50112 "ATRFacturaVentaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            column(No_; "No.")
            {
            }

            column(xCustAddr1; xCustAddr[1])
            {
            }

            column(xCustAddr2; xCustAddr[2])
            {
            }
            column(xShipAddr1; xShipAddr[1])
            {
            }

            column(xShipAddr2; xShipAddr[2])
            {
            }
            //  column(Number; Number)
            // {

            // }
            dataitem("Sales Invoice Line"; "Sales Invoice Line")
            {
                column(LineNo_SalesInvoiceLine; "Line No.")
                {
                }
            }
            trigger OnAfterGetRecord()
            begin
                cuFormatAddress.SalesInvSellTo(xCustAddr, "Sales Invoice Header");
                cuFormatAddress.SalesInvShipTo(xShipAddr, xCustAddr, "Sales Invoice Header");
                // rCompanyInfo.get;
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        xCustAddr: array[8] of Text;
        xShipAddr: array[8] of Text;
        cuFormatAddress: Codeunit "Format Address";
}