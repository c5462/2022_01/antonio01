pageextension 50112 "ATRTCNExtPosteSalesShipmentC01" extends "Posted Sales Shipment"
{
    actions
    {
        addlast(Reporting)
        {
            action(ATREtiquetasC01)
            {
                ApplicationArea = all;
                trigger OnAction()
                var
                    replEtiquetas: Report ATRTCNEtiquetaC01;
                    rlSalesShipmentLine: Record "Sales Shipment Line";
                begin
                    rlSalesShipmentLine.SetRange("Document No.", Rec."No.");
                    replEtiquetas.SetTableView(rlSalesShipmentLine);
                    replEtiquetas.RunModal();
                end;
            }
        }
    }
}
