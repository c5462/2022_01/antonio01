page 50117 "ATRTCNFichaCabPlanVacunaC01"
{
    Caption = 'Cabecera Planificada Vacunacion';
    PageType = Document;
    SourceTable = ATRTCNCabeceraPlanVacunaC01;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Codigo Vacuna field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripcion Vacuna field.';
                    ApplicationArea = All;
                }
                field("FechaIncioVacu.Planif."; Rec."FechaIncioVacu.Planif.")
                {
                    ToolTip = 'Specifies the value of the Fecha Incio Vacunación Planificada field.';
                    ApplicationArea = All;
                }
                field(Empresavacunadora; Rec.Empresavacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre Empresa Vacunadora';
                    ApplicationArea = All;
                    ToolTip = 'Aqui aparecerá el nombre del proveedor que realizará la vacunación.';
                }
            }

            part(Lineas; ATRTCNSubPagLinPlanVacC01)
            {
                ApplicationArea = all;
                SubPageLink = Codigo = field(Codigo);
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(EjemploVariablesES)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de la funcion de variables entarda salida';
                trigger OnAction()
                var
                    culATRTCNVariablesGlobalesC01: Codeunit ATRTCNVariablesGlobalesC01;
                    xlTexto: Text;
                begin
                    xlTexto := 'Antonio';
                    culATRTCNVariablesGlobalesC01.EjemploVariablesEntradaSalidaF(xlTexto);
                    Message(xlTexto);
                end;
            }
            action(EjemploVariables02ES)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de la funcion de variables entarda salida 2 text builder';
                trigger OnAction()
                var
                    culATRTCNVariablesGlobalesC01: Codeunit ATRTCNVariablesGlobalesC01;
                    xlTexto: TextBuilder;
                begin
                    xlTexto.Append('Antonio');
                    culATRTCNVariablesGlobalesC01.EjemploVariablesEntradaSalida02F(xlTexto);
                    Message(xlTexto.ToText());
                end;
            }
            action(Bingo)
            {
                ApplicationArea = All;

                trigger OnAction()
                var
                    culATRTCNVariablesGlobalesC01: Codeunit ATRTCNVariablesGlobalesC01;
                begin
                    culATRTCNVariablesGlobalesC01.BingoF();
                end;
            }
            action(PruebaFncSegundoPlano)
            {
                Caption = 'Prueba funcion en segundo plano';
                ApplicationArea = All;
                trigger OnAction()
                var
                    xlSesionInicidada: Integer;
                begin
                    //Codeunit.Run(Codeunit::ATRTCNCodeUnitLentaC01);
                    StartSession(xlSesionInicidada, Codeunit::ATRTCNCodeUnitLentaC01);
                    Message('Proceso lanzado');
                end;
            }
            action(Logs)
            {
                Caption = 'Logs';
                ApplicationArea = All;
                RunObject = page ATRTCNLogsC01;
            }
            action(CrearLineas)
            {
                Caption = 'Crear Lineas';
                ApplicationArea = All;
                Image = Create;
                trigger OnAction()
                var
                    rlATRTCNLinPlanVacunC01: Record ATRTCNLinPlanVacunC01;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlATRTCNLinPlanVacunC01.SetRange(codigo, Rec.Codigo);
                    rlATRTCNLinPlanVacunC01.ModifyAll(FechaVacunacion, Today, true);
                    Message('Tiempo: %1', CurrentDateTime - xlInicio);
                end;
            }
            action(CrearLineasBulk)
            {
                Caption = 'Crear Lineas Bulk';
                ApplicationArea = All;
                Image = Create;
                trigger OnAction()
                var
                    rlATRTCNLinPlanVacunC01: Record ATRTCNLinPlanVacunC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    rlATRTCNLinPlanVacunC01.SetRange(Codigo, Rec.Codigo);
                    if rlATRTCNLinPlanVacunC01.FindSet(true, false) then begin
                        repeat
                            rlATRTCNLinPlanVacunC01.Validate(FechaVacunacion, Today + 1);
                            rlATRTCNLinPlanVacunC01.Modify(true);
                        until rlATRTCNLinPlanVacunC01.Next() = 0;
                    end;
                    Message('Tiempo: %1', CurrentDateTime - xlInicio);
                end;
            }
            action(EjecutarTareaPrimerPlano)
            {
                ApplicationArea = All;

                trigger OnAction()
                var
                    xlSesionInicidada: Integer;
                begin
                    Codeunit.Run(Codeunit::ATRTCNCodeUnitLentaC01);
                    //StartSession(xlSesionInicidada, Codeunit::ATRTCNCodeUnitLentaC01);
                    Message('Proceso lanzado');
                end;
            }
        }
    }
}
