table 50111 "ATRTCNLinPaqueteC01"
{
    Caption = 'Lin. paquete';
    DataClassification = SystemMetadata;

    fields
    {
        field(10; IdPaquete; Code[20])
        {
            DataClassification = SystemMetadata;
            Caption = 'Id paquete';
        }

        field(15; NumLinea; Integer)
        {
            DataClassification = SystemMetadata;
            Caption = 'Nº Línea';
        }

        field(20; CodProducto; Code[20])
        {
            DataClassification = SystemMetadata;
            Caption = 'Cód. Producto';
            TableRelation = Item."No.";
        }

        field(30; CodVariante; Code[10])
        {
            DataClassification = SystemMetadata;
            Caption = 'Cód. Variante';
            TableRelation = "Item Variant".Code where("Item No." = field(CodProducto));
        }
    }

    keys
    {
        key(PK; IdPaquete, NumLinea)
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}