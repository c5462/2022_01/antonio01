pageextension 50115 "ATRTCNVendorCardExtC01" extends "Vendor Card"
{
    actions
    {
        addlast(processing)
        {
            action(ATRProveedorAClienteC01)
            {
                ApplicationArea = All;
                Caption = 'Pasar Proveedor a Cliente';
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                begin
                    rlCustomer.Init();
                    rlCustomer.TransferFields(Rec, true, true);
                    rlCustomer.Insert(true);
                    Message('Proveedor %1 creado como cliente', Rec.Name);
                    Page.Run(0, rlCustomer);
                end;
            }
        }
    }
}
