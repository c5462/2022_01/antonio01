page 50113 "ATRTCNConfiguracionC01"
{
    Caption = 'Configuración de la app 1 del curso';
    PageType = Card;
    SourceTable = ATRTCNConfiguracionC01;
    AboutTitle = 'En esta pagina crearemos la configuracion de la primera app del curso';
    AboutText = 'Configure la app de manera correcta asegurandose de que asgina el valor del campo de cliente web';
    AdditionalSearchTerms = 'Mi primera app';
    ApplicationArea = all;
    UsageCategory = Administration;
    DeleteAllowed = false;
    InsertAllowed = false;
    ShowFilter = true;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Code. Cliente web';
                    AboutText = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web';
                    Importance = Promoted;
                }
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto Registro field.';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Registro';
                    AboutText = 'Especifique el registro que se utilizará.';
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                    ApplicationArea = All;
                }
                field(Password; Password)
                {
                    Caption = ' Psw';
                    ApplicationArea = all;
                }
                field(NumIteraciones; xNumIteraciones)
                {
                    Caption = 'Nº de iteraciones';
                    ApplicationArea = all;
                }
            }
            group(InfInterna)
            {
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo de tabla field.';
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the Codigo de tabla field.';
                    ApplicationArea = All;
                }

                field(NombreTabla; Rec.NombreTablaf(rec.TipoTabla, rec.CodTabla))
                {
                    Caption = 'Nombre tabla';
                    ToolTip = 'Nombre de la tabla';
                    ApplicationArea = All;

                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    ToolTip = 'Specifies the value of the Colores del fondo field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    ToolTip = 'Specifies the value of the Colores de letra field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
            group(CamposCalculados)
            {
                Caption = 'Campos Calculados';
                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Nos muestra el nombre del cliente web';
                    ApplicationArea = all;

                }

                field(NombreClienteporFuncion; Rec.NombreClientef(Rec.CodCteWeb))
                {
                    Caption = 'Nombre Cliente';
                    ApplicationArea = all;
                }
                field(NumUnidadesVendidas; Rec.UnidadesDisponibles)
                {
                    Caption = 'Unidades disponibles';
                    ApplicationArea = all;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Ejemplo01)
            {
                ApplicationArea = all;
                Caption = 'Mensaje de acción';
                Image = AboutNav;
                trigger OnAction()
                begin
                    Message('Accion pulsada');
                end;
            }
            action(PedirDatos01)
            {
                ApplicationArea = all;
                Caption = 'Pedir datos varios';
                trigger OnAction()
                var
                    pglATRTCNEntradaDeDatosC01: Page ATRTCNEntradaDeDatosC01;
                    xlBool: Boolean;
                begin
                    pglATRTCNEntradaDeDatosC01.campoF('Ciudad nacimiento', 'Albacete');
                    pglATRTCNEntradaDeDatosC01.campoF('Pais Nacimiento', 'España');
                    pglATRTCNEntradaDeDatosC01.campoF('Comunidad Autonoma', 'CLM');
                    pglATRTCNEntradaDeDatosC01.campoF('Direccion', '');
                    pglATRTCNEntradaDeDatosC01.campoF('Cuota Primer Trimestre', true);
                    pglATRTCNEntradaDeDatosC01.campoF('Cuota Segundo Trimestre', true);
                    pglATRTCNEntradaDeDatosC01.campoF('Decimal 1', 0.2);
                    pglATRTCNEntradaDeDatosC01.campoF('Decimal 1', 0.5);

                    if pglATRTCNEntradaDeDatosC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
                        Message(pglATRTCNEntradaDeDatosC01.CampoF());
                        Message(pglATRTCNEntradaDeDatosC01.CampoF());
                        Message(pglATRTCNEntradaDeDatosC01.CampoF());
                        Message(pglATRTCNEntradaDeDatosC01.CampoF());
                        Message('Cuota 1 %1', pglATRTCNEntradaDeDatosC01.CampoF(xlBool));
                        pglATRTCNEntradaDeDatosC01.CampoF(xlBool);
                        Message('Cuota 2 %1', xlBool);

                    end else begin
                        Message('Proceso cancelado por el usuario');
                    end;
                end;
            }
            action(CambiarPass)
            {
                ApplicationArea = all;
                Caption = 'Cambiar Pass';
                trigger OnAction()
                begin
                    CambiarPasswF();
                end;
            }
            action(leftRight)
            {
                ApplicationArea = all;
                Caption = 'RightLeft';
                trigger OnAction()
                var
                    cdlATRTCNVariablesGlobalesC01: Codeunit ATRTCNVariablesGlobalesC01;
                begin
                    Message(cdlATRTCNVariablesGlobalesC01.LeftF('ABC', 2));
                    Message(cdlATRTCNVariablesGlobalesC01.RightF('ABCDEF', 2));
                end;
            }
            action(ExportarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Exportar Vacunas';
                trigger OnAction()
                begin
                    ExportarVacunasF();
                end;
            }
            action(ImportarVacunas)
            {
                ApplicationArea = all;
                Caption = 'Importar Vacunas';
                trigger OnAction()
                begin
                    ImportarVacuansF();
                end;
            }
            action(FiltroClientesAlmacenGris)
            {
                ApplicationArea = all;
                Caption = 'Filtrado de clientes en almacen gris';
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    xlFiltroGRupo: Integer;
                    rlCompany: Record Company;
                begin

                    if AccionAceptadaF(page.RunModal(Page::Companies)) then begin
                        rlCustomer.ChangeCompany(rlCompany.Name);
                        xlFiltroGRupo := rlCustomer.FilterGroup;
                        rlCustomer.FilterGroup(xlFiltroGRupo + 20);
                        rlCustomer.SetRange("Location Code", 'GRIS');
                        rlCustomer.FilterGroup;
                        if AccionAceptadaF(page.RunModal(0, rlCustomer)) then begin
                            Message('Proceso OK');
                        end else begin
                            Message('Proceso Cancelado');
                        end;
                    end else begin
                        Message('PRoceso cancelado por usuario');
                    end;
                end;
            }
            action(PruebaTxt)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con text';
                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: Text;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumIteraciones do begin
                        xlTxt += 'Antonio';
                    end;
                    Message('Tiempo invertido: %1', CurrentDateTime - xlInicio);
                end;
            }
            action(PruebaTextBuilder)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con text builder';
                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: TextBuilder;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumIteraciones do begin
                        xlTxt.Append('Antonio');
                    end;
                    Message('Tiempo invertido: %1', CurrentDateTime - xlInicio);
                end;
            }
            action(ContarClientes)
            {
                ApplicationArea = All;
                Caption = 'Contar Clientes';
                trigger OnAction()
                var
                    xlFilterPageBuilder: FilterPageBuilder;
                    clMovs: Label 'Movimientos';
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlLista: List of [Code[20]];
                begin
                    xlFilterPageBuilder.PageCaption('Seleccione los filtros');
                    xlFilterPageBuilder.AddRecord(clMovs, rlCustLedgerEntry); //añadimos el dataset
                    xlFilterPageBuilder.AddField(clMovs, rlCustLedgerEntry."Sell-to Customer No."); //añadimos los filtros
                    if xlFilterPageBuilder.RunModal() then begin //mostramos la pagina
                        rlCustLedgerEntry.SetView(xlFilterPageBuilder.GetView(clMovs, true)); //aplicamos los filtros
                        rlCustLedgerEntry.SetLoadFields("Sell-to Customer No."); //solo se puede hacer si no se modifica la bbdd.
                        if rlCustLedgerEntry.FindSet(false) then begin
                            repeat
                                if not xlLista.Contains(rlCustLedgerEntry."Sell-to Customer No.") then begin
                                    xlLista.Add(rlCustLedgerEntry."Sell-to Customer No.");
                                end;
                            until rlCustLedgerEntry.Next() = 0;
                        end;
                        Message('Hay %1 clientes', xlLista.Count);
                    end else begin
                        Error('Proceso cancelado');
                    end;
                end;
            }
        }
    }

    var
        Password: Text;
        xNumIteraciones: Integer;

    trigger OnOpenPage()
    begin
        Rec.Getf();
        EjemploNotificacionF();
    end;

    [NonDebuggable]
    procedure CambiarPasswF()
    var
        pglATRTCNEntradaDeDatosC01: Page ATRTCNEntradaDeDatosC01;
        xlNulo: Text;
    begin
        pglATRTCNEntradaDeDatosC01.CampoF('Psw Actual:', '', true);
        pglATRTCNEntradaDeDatosC01.CampoF('Psw Nueva:', '', true);
        pglATRTCNEntradaDeDatosC01.CampoF('Repita New Psw:', '', true);
        if pglATRTCNEntradaDeDatosC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            if pglATRTCNEntradaDeDatosC01.CampoF(xlNulo) = Rec.PasswordF() then begin
                if pglATRTCNEntradaDeDatosC01.CampoF(xlNulo) = pglATRTCNEntradaDeDatosC01.CampoF(xlNulo) then begin
                    Rec.PasswordF(xlNulo);
                    Message('Password Cambiada');
                end else begin
                    Message('Password actual no coidcide');
                end;
            end else begin
                Message('Password actual no coidcide');
            end;
        end else begin
            Message('Proceso cancelado por el usuario');
        end;
    end;

    local procedure ExportarVacunasF()
    var
        rlATRTCNCabeceraPlanVacunaC01: Record ATRTCNCabeceraPlanVacunaC01;
        pglATRTCNListaCabPlanVacC01: Page ATRTCNListaCabPlanVacC01;
        rlATRTCNLinPlanVacunC01: Record ATRTCNLinPlanVacunC01;
        xInStream: InStream;
        xOutStream: OutStream;
        TempLATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        clSeparator: Label '·';
        xlNombreFichero: Text;

    begin
        Message('Selecciones vacunas a exportar');
        xlNombreFichero := 'Lista Vacunacion';
        pglATRTCNListaCabPlanVacC01.LookupMode(true);
        pglATRTCNListaCabPlanVacC01.SetRecord(rlATRTCNCabeceraPlanVacunaC01);
        if pglATRTCNListaCabPlanVacC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            pglATRTCNListaCabPlanVacC01.GetSelectionFilterF(rlATRTCNCabeceraPlanVacunaC01);
            //Obtenemos el dataset
            if rlATRTCNCabeceraPlanVacunaC01.FindSet(false) then begin
                TempLATRTCNConfiguracionC01.MiBlob.CreateOutStream(xOutStream, TextEncoding::UTF8);
                TempLATRTCNConfiguracionC01.MiBlob.CreateInStream(xInStream);
                repeat
                    xOutStream.WriteText('C');
                    xOutStream.WriteText(clSeparator);
                    xOutStream.WriteText(rlATRTCNCabeceraPlanVacunaC01.Empresavacunadora);
                    xOutStream.WriteText(clSeparator);
                    xOutStream.WriteText(rlATRTCNCabeceraPlanVacunaC01.Codigo);
                    xOutStream.WriteText(clSeparator);
                    xOutStream.WriteText(rlATRTCNCabeceraPlanVacunaC01.Descripcion);
                    xOutStream.WriteText(clSeparator);
                    xOutStream.WriteText(Format((rlATRTCNCabeceraPlanVacunaC01."FechaIncioVacu.Planif.")));
                    xOutStream.WriteText(clSeparator);
                    xOutStream.WriteText(rlATRTCNCabeceraPlanVacunaC01.NombreEmpresaVacunadoraF());
                    xOutStream.WriteText(clSeparator);
                    xOutStream.WriteText();
                    rlATRTCNLinPlanVacunC01.SetRange(Codigo, rlATRTCNCabeceraPlanVacunaC01.Codigo);
                    if rlATRTCNLinPlanVacunC01.FindSet(false) then begin
                        repeat
                            xOutStream.WriteText('L');
                            xOutStream.WriteText(clSeparator);
                            xOutStream.WriteText(rlATRTCNLinPlanVacunC01.ClienteAVacunar);
                            xOutStream.WriteText(clSeparator);
                            xOutStream.WriteText(Format(rlATRTCNLinPlanVacunC01.FechaVacunacion));
                            xOutStream.WriteText(clSeparator);
                            xOutStream.WriteText(rlATRTCNLinPlanVacunC01.CodigoVacuna);
                            xOutStream.WriteText(clSeparator);
                            xOutStream.WriteText(rlATRTCNLinPlanVacunC01.CodigoOtros);
                            xOutStream.WriteText(clSeparator);
                            xOutStream.WriteText(Format(rlATRTCNLinPlanVacunC01.FechaSiguienteVacunacion));
                            xOutStream.WriteText(clSeparator);
                            xOutStream.WriteText();
                            if not DownloadFromStream(xInStream, '', '', '', xlNombreFichero) then begin
                                Error('Fichero no descargado');
                            end;
                        until rlATRTCNLinPlanVacunC01.Next() = 0;
                    end;
                until rlATRTCNCabeceraPlanVacunaC01.Next() = 0;
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    local procedure ImportarVacuansF()
    var
        xlInstr: Instream;
        xLinea: Text;
        rlATRTCNCabeceraPlanVacunaC01: Record ATRTCNCabeceraPlanVacunaC01;
        TempLATRTCNConfiguracionC01: Record ATRTCNConfiguracionC01 temporary;
        rlATRTCNLinPlanVacunC01: Record ATRTCNLinPlanVacunC01;
        ClSeparator: Label '·';
        xlFecha: Date;
        xlLinea: Integer;
    begin
        TempLATRTCNConfiguracionC01.MiBlob.CreateInStream(xlInstr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInstr) then begin
            while xlInstr.EOS do begin
                xlInstr.ReadText(xLinea);
                case true of
                    xLinea[1] = 'C':
                        begin
                            rlATRTCNCabeceraPlanVacunaC01.Init();
                            rlATRTCNCabeceraPlanVacunaC01.Validate(Codigo, xLinea.Split(ClSeparator).Get(2));
                            rlATRTCNCabeceraPlanVacunaC01.Insert(true);
                            rlATRTCNCabeceraPlanVacunaC01.Validate(Descripcion, xLinea.Split(ClSeparator).Get(3));
                            rlATRTCNCabeceraPlanVacunaC01.Validate(Empresavacunadora, xLinea.Split(ClSeparator).Get(4));
                            if Evaluate(xlFecha, xLinea.Split(ClSeparator).Get(5)) then begin
                                rlATRTCNCabeceraPlanVacunaC01.Validate("FechaIncioVacu.Planif.", xlFecha);
                            end;
                            rlATRTCNCabeceraPlanVacunaC01.Modify(true);
                        end;
                    xLinea[1] = 'L':
                        begin
                            rlATRTCNLinPlanVacunC01.Init();
                            rlATRTCNLinPlanVacunC01.Validate(Codigo, xLinea.Split(ClSeparator).Get(2));
                            if Evaluate(xlLinea, xLinea.Split(ClSeparator).Get(3)) then begin
                                rlATRTCNLinPlanVacunC01.Validate("NºLinea", xlLinea);
                            end;
                            rlATRTCNCabeceraPlanVacunaC01.Insert(true);
                            rlATRTCNLinPlanVacunC01.Validate(ClienteAVacunar, xLinea.Split(ClSeparator).Get(4));
                            rlATRTCNLinPlanVacunC01.Validate(CodigoVacuna, xLinea.Split(ClSeparator).Get(5));
                            rlATRTCNLinPlanVacunC01.Validate(CodigoOtros, xLinea.Split(ClSeparator).Get(6));
                            if Evaluate(xlFecha, xLinea.Split(ClSeparator).Get(7)) then begin
                                rlATRTCNLinPlanVacunC01.Validate(FechaVacunacion, xlFecha);
                            end;
                            if Evaluate(xlFecha, xLinea.Split(ClSeparator).Get(8)) then begin
                                rlATRTCNLinPlanVacunC01.Validate(FechaSiguienteVacunacion, xlFecha);
                            end;
                            rlATRTCNCabeceraPlanVacunaC01.Modify(true);
                        end;
                    else
                        Error('Tipo linea %1 no reconocido', xLinea);
                end
            end;
        end else begin

        end;
    end;

    procedure AccionAceptadaF(pAccion: Action) returnValue: Boolean
    begin
        returnValue := pAccion in [Action::LookupOK, Action::OK, Action::Yes];
    end;

    local procedure EjemploNotificacionF()
    var
        xlNotificacion: Notification;
        i: Integer;
    begin
        for i := 1 to 3 do begin
            xlNotificacion.Id(CreateGuid());
            xlNotificacion.Message(StrSubstNo('Carga %1 finalizada', i));
            xlNotificacion.Send();
        end;
        xlNotificacion.Id(CreateGuid());
        xlNotificacion.Message('El cliente no tiene ninguna vacuna');
        xlNotificacion.AddAction('Abrir lista clientes', Codeunit::ATRTCNVariablesGlobalesC01, 'AbrirListaClientesF');
        xlNotificacion.SetData('CodigoCliente', '10000');
        xlNotificacion.Send();
    end;


}
